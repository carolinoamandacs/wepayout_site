-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: 07-Set-2019 às 00:44
-- Versão do servidor: 5.7.24
-- versão do PHP: 7.2.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `projetos_wepayout_site`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `wp_aiowps_events`
--

DROP TABLE IF EXISTS `wp_aiowps_events`;
CREATE TABLE IF NOT EXISTS `wp_aiowps_events` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `event_type` varchar(150) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `username` varchar(150) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `event_date` datetime NOT NULL DEFAULT '1000-10-10 10:00:00',
  `ip_or_host` varchar(100) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `referer_info` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `country_code` varchar(50) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `event_data` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `wp_aiowps_failed_logins`
--

DROP TABLE IF EXISTS `wp_aiowps_failed_logins`;
CREATE TABLE IF NOT EXISTS `wp_aiowps_failed_logins` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `user_login` varchar(150) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `failed_login_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `login_attempt_ip` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `wp_aiowps_global_meta`
--

DROP TABLE IF EXISTS `wp_aiowps_global_meta`;
CREATE TABLE IF NOT EXISTS `wp_aiowps_global_meta` (
  `meta_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `date_time` datetime NOT NULL DEFAULT '1000-10-10 10:00:00',
  `meta_key1` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `meta_key2` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `meta_key3` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `meta_key4` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `meta_key5` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `meta_value1` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `meta_value2` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `meta_value3` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `meta_value4` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `meta_value5` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  PRIMARY KEY (`meta_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `wp_aiowps_login_activity`
--

DROP TABLE IF EXISTS `wp_aiowps_login_activity`;
CREATE TABLE IF NOT EXISTS `wp_aiowps_login_activity` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `user_login` varchar(150) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `login_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `logout_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `login_ip` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `login_country` varchar(150) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `browser_type` varchar(150) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `wp_aiowps_login_activity`
--

INSERT INTO `wp_aiowps_login_activity` (`id`, `user_id`, `user_login`, `login_date`, `logout_date`, `login_ip`, `login_country`, `browser_type`) VALUES
(1, 1, 'wepayout', '2019-09-03 16:23:07', '0000-00-00 00:00:00', '::1', '', ''),
(2, 1, 'wepayout', '2019-09-03 16:38:57', '0000-00-00 00:00:00', '::1', '', ''),
(3, 1, 'wepayout', '2019-09-04 13:49:27', '0000-00-00 00:00:00', '::1', '', '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `wp_aiowps_login_lockdown`
--

DROP TABLE IF EXISTS `wp_aiowps_login_lockdown`;
CREATE TABLE IF NOT EXISTS `wp_aiowps_login_lockdown` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `user_login` varchar(150) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `lockdown_date` datetime NOT NULL DEFAULT '1000-10-10 10:00:00',
  `release_date` datetime NOT NULL DEFAULT '1000-10-10 10:00:00',
  `failed_login_ip` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `lock_reason` varchar(128) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `unlock_key` varchar(128) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `wp_aiowps_permanent_block`
--

DROP TABLE IF EXISTS `wp_aiowps_permanent_block`;
CREATE TABLE IF NOT EXISTS `wp_aiowps_permanent_block` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `blocked_ip` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `block_reason` varchar(128) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `country_origin` varchar(50) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `blocked_date` datetime NOT NULL DEFAULT '1000-10-10 10:00:00',
  `unblock` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `wp_commentmeta`
--

DROP TABLE IF EXISTS `wp_commentmeta`;
CREATE TABLE IF NOT EXISTS `wp_commentmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `comment_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`meta_id`),
  KEY `comment_id` (`comment_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `wp_comments`
--

DROP TABLE IF EXISTS `wp_comments`;
CREATE TABLE IF NOT EXISTS `wp_comments` (
  `comment_ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `comment_post_ID` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `comment_author` tinytext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_author_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`comment_ID`),
  KEY `comment_post_ID` (`comment_post_ID`),
  KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  KEY `comment_date_gmt` (`comment_date_gmt`),
  KEY `comment_parent` (`comment_parent`),
  KEY `comment_author_email` (`comment_author_email`(10))
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `wp_comments`
--

INSERT INTO `wp_comments` (`comment_ID`, `comment_post_ID`, `comment_author`, `comment_author_email`, `comment_author_url`, `comment_author_IP`, `comment_date`, `comment_date_gmt`, `comment_content`, `comment_karma`, `comment_approved`, `comment_agent`, `comment_type`, `comment_parent`, `user_id`) VALUES
(1, 1, 'Um comentarista do WordPress', 'wapuu@wordpress.example', 'https://wordpress.org/', '', '2019-09-03 15:30:16', '2019-09-03 18:30:16', 'Olá, isso é um comentário.\nPara começar a moderar, editar e excluir comentários, visite a tela de Comentários no painel.\nAvatares de comentaristas vêm a partir do <a href=\"https://gravatar.com\">Gravatar</a>.', 0, '1', '', '', 0, 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `wp_links`
--

DROP TABLE IF EXISTS `wp_links`;
CREATE TABLE IF NOT EXISTS `wp_links` (
  `link_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `link_url` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) UNSIGNED NOT NULL DEFAULT '1',
  `link_rating` int(11) NOT NULL DEFAULT '0',
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `link_rss` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`link_id`),
  KEY `link_visible` (`link_visible`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `wp_options`
--

DROP TABLE IF EXISTS `wp_options`;
CREATE TABLE IF NOT EXISTS `wp_options` (
  `option_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `option_name` varchar(191) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `autoload` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'yes',
  PRIMARY KEY (`option_id`),
  UNIQUE KEY `option_name` (`option_name`)
) ENGINE=MyISAM AUTO_INCREMENT=246 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `wp_options`
--

INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'siteurl', 'http://localhost/projetos/wepayout_site', 'yes'),
(2, 'home', 'http://localhost/projetos/wepayout_site', 'yes'),
(3, 'blogname', 'We Payout', 'yes'),
(4, 'blogdescription', 'Só mais um site WordPress', 'yes'),
(5, 'users_can_register', '0', 'yes'),
(6, 'admin_email', 'carolinoamandacs@gmail.com', 'yes'),
(7, 'start_of_week', '0', 'yes'),
(8, 'use_balanceTags', '0', 'yes'),
(9, 'use_smilies', '1', 'yes'),
(10, 'require_name_email', '1', 'yes'),
(11, 'comments_notify', '1', 'yes'),
(12, 'posts_per_rss', '10', 'yes'),
(13, 'rss_use_excerpt', '0', 'yes'),
(14, 'mailserver_url', 'mail.example.com', 'yes'),
(15, 'mailserver_login', 'login@example.com', 'yes'),
(16, 'mailserver_pass', 'password', 'yes'),
(17, 'mailserver_port', '110', 'yes'),
(18, 'default_category', '1', 'yes'),
(19, 'default_comment_status', 'open', 'yes'),
(20, 'default_ping_status', 'open', 'yes'),
(21, 'default_pingback_flag', '0', 'yes'),
(22, 'posts_per_page', '10', 'yes'),
(23, 'date_format', 'j \\d\\e F \\d\\e Y', 'yes'),
(24, 'time_format', 'H:i', 'yes'),
(25, 'links_updated_date_format', 'j \\d\\e F \\d\\e Y, H:i', 'yes'),
(26, 'comment_moderation', '0', 'yes'),
(27, 'moderation_notify', '1', 'yes'),
(28, 'permalink_structure', '/%year%/%monthnum%/%day%/%postname%/', 'yes'),
(29, 'rewrite_rules', 'a:113:{s:11:\"^wp-json/?$\";s:22:\"index.php?rest_route=/\";s:14:\"^wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:21:\"^index.php/wp-json/?$\";s:22:\"index.php?rest_route=/\";s:24:\"^index.php/wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:11:\"destaque/?$\";s:28:\"index.php?post_type=destaque\";s:41:\"destaque/feed/(feed|rdf|rss|rss2|atom)/?$\";s:45:\"index.php?post_type=destaque&feed=$matches[1]\";s:36:\"destaque/(feed|rdf|rss|rss2|atom)/?$\";s:45:\"index.php?post_type=destaque&feed=$matches[1]\";s:28:\"destaque/page/([0-9]{1,})/?$\";s:46:\"index.php?post_type=destaque&paged=$matches[1]\";s:47:\"category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:42:\"category/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:23:\"category/(.+?)/embed/?$\";s:46:\"index.php?category_name=$matches[1]&embed=true\";s:35:\"category/(.+?)/page/?([0-9]{1,})/?$\";s:53:\"index.php?category_name=$matches[1]&paged=$matches[2]\";s:17:\"category/(.+?)/?$\";s:35:\"index.php?category_name=$matches[1]\";s:44:\"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:39:\"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:20:\"tag/([^/]+)/embed/?$\";s:36:\"index.php?tag=$matches[1]&embed=true\";s:32:\"tag/([^/]+)/page/?([0-9]{1,})/?$\";s:43:\"index.php?tag=$matches[1]&paged=$matches[2]\";s:14:\"tag/([^/]+)/?$\";s:25:\"index.php?tag=$matches[1]\";s:45:\"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:40:\"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:21:\"type/([^/]+)/embed/?$\";s:44:\"index.php?post_format=$matches[1]&embed=true\";s:33:\"type/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?post_format=$matches[1]&paged=$matches[2]\";s:15:\"type/([^/]+)/?$\";s:33:\"index.php?post_format=$matches[1]\";s:36:\"destaque/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:46:\"destaque/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:66:\"destaque/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:61:\"destaque/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:61:\"destaque/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:42:\"destaque/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:25:\"destaque/([^/]+)/embed/?$\";s:41:\"index.php?destaque=$matches[1]&embed=true\";s:29:\"destaque/([^/]+)/trackback/?$\";s:35:\"index.php?destaque=$matches[1]&tb=1\";s:49:\"destaque/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?destaque=$matches[1]&feed=$matches[2]\";s:44:\"destaque/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?destaque=$matches[1]&feed=$matches[2]\";s:37:\"destaque/([^/]+)/page/?([0-9]{1,})/?$\";s:48:\"index.php?destaque=$matches[1]&paged=$matches[2]\";s:44:\"destaque/([^/]+)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?destaque=$matches[1]&cpage=$matches[2]\";s:33:\"destaque/([^/]+)(?:/([0-9]+))?/?$\";s:47:\"index.php?destaque=$matches[1]&page=$matches[2]\";s:25:\"destaque/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:35:\"destaque/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:55:\"destaque/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:50:\"destaque/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:50:\"destaque/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:31:\"destaque/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:48:\".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$\";s:18:\"index.php?feed=old\";s:20:\".*wp-app\\.php(/.*)?$\";s:19:\"index.php?error=403\";s:18:\".*wp-register.php$\";s:23:\"index.php?register=true\";s:32:\"feed/(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:27:\"(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:8:\"embed/?$\";s:21:\"index.php?&embed=true\";s:20:\"page/?([0-9]{1,})/?$\";s:28:\"index.php?&paged=$matches[1]\";s:27:\"comment-page-([0-9]{1,})/?$\";s:38:\"index.php?&page_id=6&cpage=$matches[1]\";s:41:\"comments/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:36:\"comments/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:17:\"comments/embed/?$\";s:21:\"index.php?&embed=true\";s:44:\"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:39:\"search/(.+)/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:20:\"search/(.+)/embed/?$\";s:34:\"index.php?s=$matches[1]&embed=true\";s:32:\"search/(.+)/page/?([0-9]{1,})/?$\";s:41:\"index.php?s=$matches[1]&paged=$matches[2]\";s:14:\"search/(.+)/?$\";s:23:\"index.php?s=$matches[1]\";s:47:\"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:42:\"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:23:\"author/([^/]+)/embed/?$\";s:44:\"index.php?author_name=$matches[1]&embed=true\";s:35:\"author/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?author_name=$matches[1]&paged=$matches[2]\";s:17:\"author/([^/]+)/?$\";s:33:\"index.php?author_name=$matches[1]\";s:69:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:45:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$\";s:74:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&embed=true\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]\";s:39:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$\";s:63:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]\";s:56:\"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:51:\"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:32:\"([0-9]{4})/([0-9]{1,2})/embed/?$\";s:58:\"index.php?year=$matches[1]&monthnum=$matches[2]&embed=true\";s:44:\"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]\";s:26:\"([0-9]{4})/([0-9]{1,2})/?$\";s:47:\"index.php?year=$matches[1]&monthnum=$matches[2]\";s:43:\"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:38:\"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:19:\"([0-9]{4})/embed/?$\";s:37:\"index.php?year=$matches[1]&embed=true\";s:31:\"([0-9]{4})/page/?([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&paged=$matches[2]\";s:13:\"([0-9]{4})/?$\";s:26:\"index.php?year=$matches[1]\";s:58:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:68:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:88:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:83:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:83:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:64:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:53:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/embed/?$\";s:91:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&embed=true\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/trackback/?$\";s:85:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&tb=1\";s:77:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:97:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&feed=$matches[5]\";s:72:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:97:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&feed=$matches[5]\";s:65:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/page/?([0-9]{1,})/?$\";s:98:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&paged=$matches[5]\";s:72:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/comment-page-([0-9]{1,})/?$\";s:98:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&cpage=$matches[5]\";s:61:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)(?:/([0-9]+))?/?$\";s:97:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&page=$matches[5]\";s:47:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:57:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:77:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:72:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:72:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:53:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/comment-page-([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&cpage=$matches[4]\";s:51:\"([0-9]{4})/([0-9]{1,2})/comment-page-([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&cpage=$matches[3]\";s:38:\"([0-9]{4})/comment-page-([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&cpage=$matches[2]\";s:27:\".?.+?/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\".?.+?/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\".?.+?/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"(.?.+?)/embed/?$\";s:41:\"index.php?pagename=$matches[1]&embed=true\";s:20:\"(.?.+?)/trackback/?$\";s:35:\"index.php?pagename=$matches[1]&tb=1\";s:40:\"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:35:\"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:28:\"(.?.+?)/page/?([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&paged=$matches[2]\";s:35:\"(.?.+?)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&cpage=$matches[2]\";s:24:\"(.?.+?)(?:/([0-9]+))?/?$\";s:47:\"index.php?pagename=$matches[1]&page=$matches[2]\";}', 'yes'),
(30, 'hack_file', '0', 'yes'),
(31, 'blog_charset', 'UTF-8', 'yes'),
(32, 'moderation_keys', '', 'no'),
(33, 'active_plugins', 'a:6:{i:0;s:35:\"redux-framework/redux-framework.php\";i:1;s:51:\"all-in-one-wp-security-and-firewall/wp-security.php\";i:2;s:33:\"base-we-payout/base-we-payout.php\";i:3;s:36:\"contact-form-7/wp-contact-form-7.php\";i:4;s:21:\"meta-box/meta-box.php\";i:5;s:37:\"post-types-order/post-types-order.php\";}', 'yes'),
(34, 'category_base', '', 'yes'),
(35, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
(36, 'comment_max_links', '2', 'yes'),
(37, 'gmt_offset', '0', 'yes'),
(38, 'default_email_category', '1', 'yes'),
(39, 'recently_edited', '', 'no'),
(40, 'template', 'we-payout', 'yes'),
(41, 'stylesheet', 'we-payout', 'yes'),
(42, 'comment_whitelist', '1', 'yes'),
(43, 'blacklist_keys', '', 'no'),
(44, 'comment_registration', '0', 'yes'),
(45, 'html_type', 'text/html', 'yes'),
(46, 'use_trackback', '0', 'yes'),
(47, 'default_role', 'subscriber', 'yes'),
(48, 'db_version', '44719', 'yes'),
(49, 'uploads_use_yearmonth_folders', '1', 'yes'),
(50, 'upload_path', '', 'yes'),
(51, 'blog_public', '0', 'yes'),
(52, 'default_link_category', '2', 'yes'),
(53, 'show_on_front', 'page', 'yes'),
(54, 'tag_base', '', 'yes'),
(55, 'show_avatars', '1', 'yes'),
(56, 'avatar_rating', 'G', 'yes'),
(57, 'upload_url_path', '', 'yes'),
(58, 'thumbnail_size_w', '150', 'yes'),
(59, 'thumbnail_size_h', '150', 'yes'),
(60, 'thumbnail_crop', '1', 'yes'),
(61, 'medium_size_w', '300', 'yes'),
(62, 'medium_size_h', '300', 'yes'),
(63, 'avatar_default', 'mystery', 'yes'),
(64, 'large_size_w', '1024', 'yes'),
(65, 'large_size_h', '1024', 'yes'),
(66, 'image_default_link_type', 'none', 'yes'),
(67, 'image_default_size', '', 'yes'),
(68, 'image_default_align', '', 'yes'),
(69, 'close_comments_for_old_posts', '0', 'yes'),
(70, 'close_comments_days_old', '14', 'yes'),
(71, 'thread_comments', '1', 'yes'),
(72, 'thread_comments_depth', '5', 'yes'),
(73, 'page_comments', '0', 'yes'),
(74, 'comments_per_page', '50', 'yes'),
(75, 'default_comments_page', 'newest', 'yes'),
(76, 'comment_order', 'asc', 'yes'),
(77, 'sticky_posts', 'a:0:{}', 'yes'),
(78, 'widget_categories', 'a:2:{i:2;a:4:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:12:\"hierarchical\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(79, 'widget_text', 'a:0:{}', 'yes'),
(80, 'widget_rss', 'a:0:{}', 'yes'),
(81, 'uninstall_plugins', 'a:0:{}', 'no'),
(82, 'timezone_string', 'America/Sao_Paulo', 'yes'),
(83, 'page_for_posts', '0', 'yes'),
(84, 'page_on_front', '6', 'yes'),
(85, 'default_post_format', '0', 'yes'),
(86, 'link_manager_enabled', '0', 'yes'),
(87, 'finished_splitting_shared_terms', '1', 'yes'),
(88, 'site_icon', '0', 'yes'),
(89, 'medium_large_size_w', '768', 'yes'),
(90, 'medium_large_size_h', '0', 'yes'),
(91, 'wp_page_for_privacy_policy', '3', 'yes'),
(92, 'show_comments_cookies_opt_in', '1', 'yes'),
(93, 'initial_db_version', '44719', 'yes'),
(94, 'wp_user_roles', 'a:5:{s:13:\"administrator\";a:2:{s:4:\"name\";s:13:\"Administrator\";s:12:\"capabilities\";a:61:{s:13:\"switch_themes\";b:1;s:11:\"edit_themes\";b:1;s:16:\"activate_plugins\";b:1;s:12:\"edit_plugins\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_files\";b:1;s:14:\"manage_options\";b:1;s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:6:\"import\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:8:\"level_10\";b:1;s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:12:\"delete_users\";b:1;s:12:\"create_users\";b:1;s:17:\"unfiltered_upload\";b:1;s:14:\"edit_dashboard\";b:1;s:14:\"update_plugins\";b:1;s:14:\"delete_plugins\";b:1;s:15:\"install_plugins\";b:1;s:13:\"update_themes\";b:1;s:14:\"install_themes\";b:1;s:11:\"update_core\";b:1;s:10:\"list_users\";b:1;s:12:\"remove_users\";b:1;s:13:\"promote_users\";b:1;s:18:\"edit_theme_options\";b:1;s:13:\"delete_themes\";b:1;s:6:\"export\";b:1;}}s:6:\"editor\";a:2:{s:4:\"name\";s:6:\"Editor\";s:12:\"capabilities\";a:34:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;}}s:6:\"author\";a:2:{s:4:\"name\";s:6:\"Author\";s:12:\"capabilities\";a:10:{s:12:\"upload_files\";b:1;s:10:\"edit_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;s:22:\"delete_published_posts\";b:1;}}s:11:\"contributor\";a:2:{s:4:\"name\";s:11:\"Contributor\";s:12:\"capabilities\";a:5:{s:10:\"edit_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;}}s:10:\"subscriber\";a:2:{s:4:\"name\";s:10:\"Subscriber\";s:12:\"capabilities\";a:2:{s:4:\"read\";b:1;s:7:\"level_0\";b:1;}}}', 'yes'),
(95, 'fresh_site', '0', 'yes'),
(96, 'WPLANG', 'pt_BR', 'yes'),
(97, 'widget_search', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(98, 'widget_recent-posts', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(99, 'widget_recent-comments', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(100, 'widget_archives', 'a:2:{i:2;a:3:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(101, 'widget_meta', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(102, 'sidebars_widgets', 'a:3:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:13:\"array_version\";i:3;}', 'yes'),
(103, 'cron', 'a:6:{i:1567794616;a:1:{s:32:\"recovery_mode_clean_expired_keys\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1567794617;a:4:{s:16:\"wp_version_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:17:\"wp_update_plugins\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:16:\"wp_update_themes\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:34:\"wp_privacy_delete_old_export_files\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"hourly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:3600;}}}i:1567794631;a:2:{s:19:\"wp_scheduled_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}s:25:\"delete_expired_transients\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1567794633;a:1:{s:30:\"wp_scheduled_auto_draft_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1567794722;a:2:{s:23:\"aiowps_daily_cron_event\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}s:24:\"aiowps_hourly_cron_event\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"hourly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:3600;}}}s:7:\"version\";i:2;}', 'yes'),
(104, 'widget_pages', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(105, 'widget_calendar', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(106, 'widget_media_audio', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(107, 'widget_media_image', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(108, 'widget_media_gallery', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(109, 'widget_media_video', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(110, 'widget_tag_cloud', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(111, 'widget_nav_menu', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(112, 'widget_custom_html', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(114, 'recovery_keys', 'a:0:{}', 'yes'),
(116, 'theme_mods_twentynineteen', 'a:2:{s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1567535861;s:4:\"data\";a:2:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}}}}', 'yes'),
(238, '_site_transient_update_core', 'O:8:\"stdClass\":4:{s:7:\"updates\";a:3:{i:0;O:8:\"stdClass\":10:{s:8:\"response\";s:7:\"upgrade\";s:8:\"download\";s:65:\"https://downloads.wordpress.org/release/pt_BR/wordpress-5.2.3.zip\";s:6:\"locale\";s:5:\"pt_BR\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:65:\"https://downloads.wordpress.org/release/pt_BR/wordpress-5.2.3.zip\";s:10:\"no_content\";b:0;s:11:\"new_bundled\";b:0;s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:5:\"5.2.3\";s:7:\"version\";s:5:\"5.2.3\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.0\";s:15:\"partial_version\";s:0:\"\";}i:1;O:8:\"stdClass\":10:{s:8:\"response\";s:7:\"upgrade\";s:8:\"download\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.2.3.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.2.3.zip\";s:10:\"no_content\";s:70:\"https://downloads.wordpress.org/release/wordpress-5.2.3-no-content.zip\";s:11:\"new_bundled\";s:71:\"https://downloads.wordpress.org/release/wordpress-5.2.3-new-bundled.zip\";s:7:\"partial\";s:69:\"https://downloads.wordpress.org/release/wordpress-5.2.3-partial-2.zip\";s:8:\"rollback\";b:0;}s:7:\"current\";s:5:\"5.2.3\";s:7:\"version\";s:5:\"5.2.3\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.0\";s:15:\"partial_version\";s:5:\"5.2.2\";}i:2;O:8:\"stdClass\":11:{s:8:\"response\";s:10:\"autoupdate\";s:8:\"download\";s:65:\"https://downloads.wordpress.org/release/pt_BR/wordpress-5.2.3.zip\";s:6:\"locale\";s:5:\"pt_BR\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:65:\"https://downloads.wordpress.org/release/pt_BR/wordpress-5.2.3.zip\";s:10:\"no_content\";b:0;s:11:\"new_bundled\";b:0;s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:5:\"5.2.3\";s:7:\"version\";s:5:\"5.2.3\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.0\";s:15:\"partial_version\";s:0:\"\";s:9:\"new_files\";s:1:\"1\";}}s:12:\"last_checked\";i:1567773940;s:15:\"version_checked\";s:5:\"5.2.2\";s:12:\"translations\";a:0:{}}', 'no'),
(125, '_site_transient_timeout_browser_471e4b86e3560c6feb474def098169b6', '1568140232', 'no'),
(126, '_site_transient_browser_471e4b86e3560c6feb474def098169b6', 'a:10:{s:4:\"name\";s:6:\"Chrome\";s:7:\"version\";s:13:\"76.0.3809.132\";s:8:\"platform\";s:7:\"Windows\";s:10:\"update_url\";s:29:\"https://www.google.com/chrome\";s:7:\"img_src\";s:43:\"http://s.w.org/images/browsers/chrome.png?1\";s:11:\"img_src_ssl\";s:44:\"https://s.w.org/images/browsers/chrome.png?1\";s:15:\"current_version\";s:2:\"18\";s:7:\"upgrade\";b:0;s:8:\"insecure\";b:0;s:6:\"mobile\";b:0;}', 'no'),
(127, '_site_transient_timeout_php_check_d005457bdf39fe8b07a9eaff24a9225e', '1568140233', 'no'),
(128, '_site_transient_php_check_d005457bdf39fe8b07a9eaff24a9225e', 'a:5:{s:19:\"recommended_version\";s:3:\"7.3\";s:15:\"minimum_version\";s:6:\"5.6.20\";s:12:\"is_supported\";b:0;s:9:\"is_secure\";b:0;s:13:\"is_acceptable\";b:0;}', 'no'),
(236, '_site_transient_timeout_theme_roots', '1567775737', 'no'),
(237, '_site_transient_theme_roots', 'a:2:{s:14:\"twentynineteen\";s:7:\"/themes\";s:9:\"we-payout\";s:7:\"/themes\";}', 'no'),
(239, '_site_transient_update_themes', 'O:8:\"stdClass\":4:{s:12:\"last_checked\";i:1567773941;s:7:\"checked\";a:2:{s:14:\"twentynineteen\";s:3:\"1.4\";s:9:\"we-payout\";s:3:\"1.0\";}s:8:\"response\";a:0:{}s:12:\"translations\";a:0:{}}', 'no'),
(150, 'aiowpsec_db_version', '1.9', 'yes'),
(151, 'aio_wp_security_configs', 'a:91:{s:19:\"aiowps_enable_debug\";s:0:\"\";s:36:\"aiowps_remove_wp_generator_meta_info\";s:0:\"\";s:25:\"aiowps_prevent_hotlinking\";s:0:\"\";s:28:\"aiowps_enable_login_lockdown\";s:0:\"\";s:28:\"aiowps_allow_unlock_requests\";s:0:\"\";s:25:\"aiowps_max_login_attempts\";s:1:\"3\";s:24:\"aiowps_retry_time_period\";s:1:\"5\";s:26:\"aiowps_lockout_time_length\";s:2:\"60\";s:28:\"aiowps_set_generic_login_msg\";s:0:\"\";s:26:\"aiowps_enable_email_notify\";s:0:\"\";s:20:\"aiowps_email_address\";s:26:\"carolinoamandacs@gmail.com\";s:27:\"aiowps_enable_forced_logout\";s:0:\"\";s:25:\"aiowps_logout_time_period\";s:2:\"60\";s:39:\"aiowps_enable_invalid_username_lockdown\";s:0:\"\";s:43:\"aiowps_instantly_lockout_specific_usernames\";a:0:{}s:32:\"aiowps_unlock_request_secret_key\";s:20:\"z1ufcpqstrjgvch8h0ag\";s:35:\"aiowps_lockdown_enable_whitelisting\";s:0:\"\";s:36:\"aiowps_lockdown_allowed_ip_addresses\";s:0:\"\";s:26:\"aiowps_enable_whitelisting\";s:0:\"\";s:27:\"aiowps_allowed_ip_addresses\";s:0:\"\";s:27:\"aiowps_enable_login_captcha\";s:0:\"\";s:34:\"aiowps_enable_custom_login_captcha\";s:0:\"\";s:31:\"aiowps_enable_woo_login_captcha\";s:0:\"\";s:34:\"aiowps_enable_woo_register_captcha\";s:0:\"\";s:38:\"aiowps_enable_woo_lostpassword_captcha\";s:0:\"\";s:25:\"aiowps_captcha_secret_key\";s:20:\"5v5zsjt5x2ith2xbxnai\";s:42:\"aiowps_enable_manual_registration_approval\";s:0:\"\";s:39:\"aiowps_enable_registration_page_captcha\";s:0:\"\";s:35:\"aiowps_enable_registration_honeypot\";s:0:\"\";s:27:\"aiowps_enable_random_prefix\";s:0:\"\";s:31:\"aiowps_enable_automated_backups\";s:0:\"\";s:26:\"aiowps_db_backup_frequency\";s:1:\"4\";s:25:\"aiowps_db_backup_interval\";s:1:\"2\";s:26:\"aiowps_backup_files_stored\";s:1:\"2\";s:32:\"aiowps_send_backup_email_address\";s:0:\"\";s:27:\"aiowps_backup_email_address\";s:26:\"carolinoamandacs@gmail.com\";s:27:\"aiowps_disable_file_editing\";s:0:\"\";s:37:\"aiowps_prevent_default_wp_file_access\";s:0:\"\";s:22:\"aiowps_system_log_file\";s:9:\"error_log\";s:26:\"aiowps_enable_blacklisting\";s:0:\"\";s:26:\"aiowps_banned_ip_addresses\";s:0:\"\";s:28:\"aiowps_enable_basic_firewall\";s:0:\"\";s:31:\"aiowps_enable_pingback_firewall\";s:0:\"\";s:38:\"aiowps_disable_xmlrpc_pingback_methods\";s:0:\"\";s:34:\"aiowps_block_debug_log_file_access\";s:0:\"\";s:26:\"aiowps_disable_index_views\";s:0:\"\";s:30:\"aiowps_disable_trace_and_track\";s:0:\"\";s:28:\"aiowps_forbid_proxy_comments\";s:0:\"\";s:29:\"aiowps_deny_bad_query_strings\";s:0:\"\";s:34:\"aiowps_advanced_char_string_filter\";s:0:\"\";s:25:\"aiowps_enable_5g_firewall\";s:0:\"\";s:25:\"aiowps_enable_6g_firewall\";s:0:\"\";s:26:\"aiowps_enable_custom_rules\";s:0:\"\";s:32:\"aiowps_place_custom_rules_at_top\";s:0:\"\";s:19:\"aiowps_custom_rules\";s:0:\"\";s:25:\"aiowps_enable_404_logging\";s:0:\"\";s:28:\"aiowps_enable_404_IP_lockout\";s:0:\"\";s:30:\"aiowps_404_lockout_time_length\";s:2:\"60\";s:28:\"aiowps_404_lock_redirect_url\";s:16:\"http://127.0.0.1\";s:31:\"aiowps_enable_rename_login_page\";s:0:\"\";s:28:\"aiowps_enable_login_honeypot\";s:0:\"\";s:43:\"aiowps_enable_brute_force_attack_prevention\";s:0:\"\";s:30:\"aiowps_brute_force_secret_word\";s:0:\"\";s:24:\"aiowps_cookie_brute_test\";s:0:\"\";s:44:\"aiowps_cookie_based_brute_force_redirect_url\";s:16:\"http://127.0.0.1\";s:59:\"aiowps_brute_force_attack_prevention_pw_protected_exception\";s:0:\"\";s:51:\"aiowps_brute_force_attack_prevention_ajax_exception\";s:0:\"\";s:19:\"aiowps_site_lockout\";s:0:\"\";s:23:\"aiowps_site_lockout_msg\";s:0:\"\";s:30:\"aiowps_enable_spambot_blocking\";s:0:\"\";s:29:\"aiowps_enable_comment_captcha\";s:0:\"\";s:31:\"aiowps_enable_autoblock_spam_ip\";s:0:\"\";s:33:\"aiowps_spam_ip_min_comments_block\";s:0:\"\";s:33:\"aiowps_enable_bp_register_captcha\";s:0:\"\";s:35:\"aiowps_enable_bbp_new_topic_captcha\";s:0:\"\";s:32:\"aiowps_enable_automated_fcd_scan\";s:0:\"\";s:25:\"aiowps_fcd_scan_frequency\";s:1:\"4\";s:24:\"aiowps_fcd_scan_interval\";s:1:\"2\";s:28:\"aiowps_fcd_exclude_filetypes\";s:0:\"\";s:24:\"aiowps_fcd_exclude_files\";s:0:\"\";s:26:\"aiowps_send_fcd_scan_email\";s:0:\"\";s:29:\"aiowps_fcd_scan_email_address\";s:26:\"carolinoamandacs@gmail.com\";s:27:\"aiowps_fcds_change_detected\";b:0;s:22:\"aiowps_copy_protection\";s:0:\"\";s:40:\"aiowps_prevent_site_display_inside_frame\";s:0:\"\";s:32:\"aiowps_prevent_users_enumeration\";s:0:\"\";s:42:\"aiowps_disallow_unauthorized_rest_requests\";s:0:\"\";s:25:\"aiowps_ip_retrieve_method\";s:1:\"0\";s:25:\"aiowps_recaptcha_site_key\";s:0:\"\";s:27:\"aiowps_recaptcha_secret_key\";s:0:\"\";s:24:\"aiowps_default_recaptcha\";s:0:\"\";}', 'yes'),
(132, 'can_compress_scripts', '1', 'no'),
(145, 'recently_activated', 'a:0:{}', 'yes'),
(170, 'redux_version_upgraded_from', '3.6.15', 'yes'),
(171, 'configuracao', 'a:34:{s:8:\"last_tab\";s:1:\"1\";s:11:\"header_logo\";a:9:{s:3:\"url\";s:75:\"http://localhost/projetos/wepayout_site/wp-content/uploads/2019/09/logo.png\";s:2:\"id\";s:2:\"21\";s:6:\"height\";s:2:\"57\";s:5:\"width\";s:3:\"139\";s:9:\"thumbnail\";s:75:\"http://localhost/projetos/wepayout_site/wp-content/uploads/2019/09/logo.png\";s:5:\"title\";s:4:\"logo\";s:7:\"caption\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}s:23:\"config_site_rodape_logo\";a:9:{s:3:\"url\";s:79:\"http://localhost/projetos/wepayout_site/wp-content/uploads/2019/09/wepayout.png\";s:2:\"id\";s:2:\"22\";s:6:\"height\";s:2:\"57\";s:5:\"width\";s:3:\"139\";s:9:\"thumbnail\";s:79:\"http://localhost/projetos/wepayout_site/wp-content/uploads/2019/09/wepayout.png\";s:5:\"title\";s:8:\"wepayout\";s:7:\"caption\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}s:28:\"config_site_rodape_copyright\";s:38:\"WePayout © 2019 | Direitos reservados\";s:33:\"config_midia_rodape_link_linkedin\";s:1:\"#\";s:33:\"config_midia_rodape_link_facebook\";s:1:\"#\";s:35:\"config_foque_servico_inicial_titulo\";s:58:\"Foque no seu negócio que nós <br> focamos nos pagamentos\";s:34:\"config_foque_servico_inicial_texto\";s:99:\"Processamos os pagamentos por você aos seus fornecedores, <br> clientes e para quem você precisar\";s:36:\"config_foque_servicos_inicial_titulo\";s:45:\"O que os serviços da WePayout <br> oferecem?\";s:35:\"config_foque_servicos_inicial_texto\";s:46:\"Pagamentos com rapidez, segurança e qualidade\";s:43:\"config_foque_servicos_inicial_titulo_mockup\";s:22:\"Integração intuitiva\";s:42:\"config_foque_servicos_inicial_texto_mockup\";s:83:\"A integração é smart e segura. Integre via API ou envie os pagamentos em excell.\";s:41:\"config_problemasResolvemos_inicial_titulo\";s:24:\"Problemas que resolvemos\";s:40:\"config_problemasResolvemos_inicial_media\";a:9:{s:3:\"url\";s:78:\"http://localhost/projetos/wepayout_site/wp-content/uploads/2019/09/image10.png\";s:2:\"id\";s:2:\"24\";s:6:\"height\";s:3:\"703\";s:5:\"width\";s:3:\"530\";s:9:\"thumbnail\";s:86:\"http://localhost/projetos/wepayout_site/wp-content/uploads/2019/09/image10-150x150.png\";s:5:\"title\";s:7:\"image10\";s:7:\"caption\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}s:32:\"config_nossoFluxo_inicial_titulo\";s:25:\"Nosso fluxo de pagamentos\";s:31:\"config_nossoFluxo_inicial_media\";s:68:\"Da sua conta bancaria para a nossa, da nossa para quem você desejar\";s:38:\"config_nossoFluxo_inicial_icone_media1\";a:9:{s:3:\"url\";s:79:\"http://localhost/projetos/wepayout_site/wp-content/uploads/2019/09/building.png\";s:2:\"id\";s:2:\"25\";s:6:\"height\";s:3:\"108\";s:5:\"width\";s:3:\"108\";s:9:\"thumbnail\";s:79:\"http://localhost/projetos/wepayout_site/wp-content/uploads/2019/09/building.png\";s:5:\"title\";s:8:\"building\";s:7:\"caption\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}s:37:\"config_nossoFluxo_inicial_icone_text1\";s:26:\"Conta bancária do cliente\";s:38:\"config_nossoFluxo_inicial_icone_media2\";a:9:{s:3:\"url\";s:78:\"http://localhost/projetos/wepayout_site/wp-content/uploads/2019/09/image28.png\";s:2:\"id\";s:2:\"26\";s:6:\"height\";s:2:\"76\";s:5:\"width\";s:2:\"66\";s:9:\"thumbnail\";s:78:\"http://localhost/projetos/wepayout_site/wp-content/uploads/2019/09/image28.png\";s:5:\"title\";s:7:\"image28\";s:7:\"caption\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}s:37:\"config_nossoFluxo_inicial_icone_text2\";s:27:\"Conta bancária da WePayout\";s:38:\"config_nossoFluxo_inicial_icone_media3\";a:9:{s:3:\"url\";s:78:\"http://localhost/projetos/wepayout_site/wp-content/uploads/2019/09/image25.png\";s:2:\"id\";s:2:\"27\";s:6:\"height\";s:3:\"108\";s:5:\"width\";s:2:\"80\";s:9:\"thumbnail\";s:78:\"http://localhost/projetos/wepayout_site/wp-content/uploads/2019/09/image25.png\";s:5:\"title\";s:7:\"image25\";s:7:\"caption\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}s:37:\"config_nossoFluxo_inicial_icone_text3\";s:22:\"Conta do Cliente Final\";s:30:\"config_pagmento_inicial_titulo\";s:34:\"Faça apenas um pagamento por dia!\";s:29:\"config_pagmento_inicial_media\";s:19:\"Veja como funciona:\";s:29:\"config_quemSomos_inicial_foto\";a:9:{s:3:\"url\";s:78:\"http://localhost/projetos/wepayout_site/wp-content/uploads/2019/09/image58.png\";s:2:\"id\";s:2:\"28\";s:6:\"height\";s:3:\"451\";s:5:\"width\";s:3:\"365\";s:9:\"thumbnail\";s:86:\"http://localhost/projetos/wepayout_site/wp-content/uploads/2019/09/image58-150x150.png\";s:5:\"title\";s:7:\"image58\";s:7:\"caption\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}s:31:\"config_quemSomos_inicial_Titulo\";s:11:\" É seguro!\";s:30:\"config_quemSomos_inicial_Texto\";s:508:\"<p>As aplicações são construídas com foco na segurança das informações que transacionamos e armazenamos, automatizando ao máximo os processos e assim inibindo a manipulação de dados.</p>\r\n\r\n<p>Seguimos protocolos de desenvolvimento estabelecidos pela OWASP Foundation, maior autoridade em padrões de segurança de sistemas.\r\n</p>\r\n<p>Clientes passam por uma due diligence completa para garantirmos que o fluxo do dinheiro do beneficiário inicial ao final esteja de acordo com padrões de PLD.</p>\";s:30:\"config_quemSomos_inicial_foto1\";a:9:{s:3:\"url\";s:76:\"http://localhost/projetos/wepayout_site/wp-content/uploads/2019/09/Group.png\";s:2:\"id\";s:2:\"29\";s:6:\"height\";s:3:\"289\";s:5:\"width\";s:3:\"564\";s:9:\"thumbnail\";s:84:\"http://localhost/projetos/wepayout_site/wp-content/uploads/2019/09/Group-150x150.png\";s:5:\"title\";s:5:\"Group\";s:7:\"caption\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}s:32:\"config_quemSomos_inicial_Titulo2\";s:11:\"Quem somos?\";s:31:\"config_quemSomos_inicial_Texto3\";s:734:\"<p>A WePayout foi fundada por profissionais com mais de 10 anos de <br> experiência na indústria de pagamentos,  TI, parcerias estratégicas<br> e comerciais no mercado nacional e crossborder.</p>\r\n					<p>Nossa equipe trouxe os melhores ingredientes para fazer a receita<br> perfeita: um produto tecnológico e diversificado. Entendemos suas<br> dores e customizamos as soluções para resolvê-las.\r\n					<p>Trabalhamos pautados em ética e qualidade, em termos uma<br> operação transparente. Buscamos crescimento sustentável<br> trazendo satisfação a nossos clientes e colaboradores.</p>\r\n					<p>Não somos mais uma fintech no mercado, somos uma empresa que<br> se preocupa com a qualidade na prestação de serviços. </p>\";s:21:\"config_contato_titulo\";s:11:\"Contate-nos\";s:20:\"config_contato_media\";a:9:{s:3:\"url\";s:76:\"http://localhost/projetos/wepayout_site/wp-content/uploads/2019/09/14610.png\";s:2:\"id\";s:2:\"30\";s:6:\"height\";s:3:\"540\";s:5:\"width\";s:4:\"1629\";s:9:\"thumbnail\";s:84:\"http://localhost/projetos/wepayout_site/wp-content/uploads/2019/09/14610-150x150.png\";s:5:\"title\";s:5:\"14610\";s:7:\"caption\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}s:20:\"config_contato_email\";s:23:\"contato@wepayout.com.br\";s:19:\"opt-customizer-only\";s:1:\"2\";}', 'yes'),
(172, 'configuracao-transients', 'a:2:{s:14:\"changed_values\";a:0:{}s:9:\"last_save\";i:1567605271;}', 'yes'),
(165, 'wpcf7', 'a:2:{s:7:\"version\";s:5:\"5.1.4\";s:13:\"bulk_validate\";a:4:{s:9:\"timestamp\";d:1567524894;s:7:\"version\";s:5:\"5.1.4\";s:11:\"count_valid\";i:1;s:13:\"count_invalid\";i:0;}}', 'yes'),
(169, 'recovery_mode_email_last_sent', '1567537325', 'yes'),
(166, 'current_theme', 'We Payout', 'yes'),
(167, 'theme_mods_we-payout', 'a:3:{i:0;b:0;s:18:\"nav_menu_locations\";a:1:{s:6:\"menu-1\";i:3;}s:18:\"custom_css_post_id\";i:-1;}', 'yes'),
(168, 'theme_switched', '', 'yes'),
(178, 'category_children', 'a:0:{}', 'yes'),
(179, '_site_transient_timeout_php_check_03bb19de23a7f39f237dfd15fa323af5', '1568144339', 'no'),
(180, '_site_transient_php_check_03bb19de23a7f39f237dfd15fa323af5', 'a:5:{s:19:\"recommended_version\";s:3:\"7.3\";s:15:\"minimum_version\";s:6:\"5.6.20\";s:12:\"is_supported\";b:1;s:9:\"is_secure\";b:1;s:13:\"is_acceptable\";b:1;}', 'no'),
(185, 'nav_menu_options', 'a:2:{i:0;b:0;s:8:\"auto_add\";a:0:{}}', 'yes'),
(240, '_site_transient_update_plugins', 'O:8:\"stdClass\":5:{s:12:\"last_checked\";i:1567773942;s:7:\"checked\";a:9:{s:19:\"akismet/akismet.php\";s:5:\"4.1.2\";s:51:\"all-in-one-wp-security-and-firewall/wp-security.php\";s:5:\"4.4.0\";s:33:\"base-we-payout/base-we-payout.php\";s:3:\"0.1\";s:36:\"contact-form-7/wp-contact-form-7.php\";s:5:\"5.1.4\";s:9:\"hello.php\";s:5:\"1.7.2\";s:21:\"meta-box/meta-box.php\";s:5:\"5.1.2\";s:37:\"post-types-order/post-types-order.php\";s:7:\"1.9.4.1\";s:35:\"redux-framework/redux-framework.php\";s:6:\"3.6.15\";s:21:\"safe-svg/safe-svg.php\";s:5:\"1.9.4\";}s:8:\"response\";a:0:{}s:12:\"translations\";a:3:{i:0;a:7:{s:4:\"type\";s:6:\"plugin\";s:4:\"slug\";s:14:\"contact-form-7\";s:8:\"language\";s:5:\"pt_BR\";s:7:\"version\";s:5:\"5.1.4\";s:7:\"updated\";s:19:\"2019-08-09 13:03:34\";s:7:\"package\";s:81:\"https://downloads.wordpress.org/translation/plugin/contact-form-7/5.1.4/pt_BR.zip\";s:10:\"autoupdate\";b:1;}i:1;a:7:{s:4:\"type\";s:6:\"plugin\";s:4:\"slug\";s:11:\"hello-dolly\";s:8:\"language\";s:5:\"pt_BR\";s:7:\"version\";s:5:\"1.7.2\";s:7:\"updated\";s:19:\"2019-08-13 18:09:11\";s:7:\"package\";s:78:\"https://downloads.wordpress.org/translation/plugin/hello-dolly/1.7.2/pt_BR.zip\";s:10:\"autoupdate\";b:1;}i:2;a:7:{s:4:\"type\";s:6:\"plugin\";s:4:\"slug\";s:8:\"meta-box\";s:8:\"language\";s:5:\"pt_BR\";s:7:\"version\";s:6:\"4.18.4\";s:7:\"updated\";s:19:\"2019-07-13 02:22:41\";s:7:\"package\";s:76:\"https://downloads.wordpress.org/translation/plugin/meta-box/4.18.4/pt_BR.zip\";s:10:\"autoupdate\";b:1;}}s:9:\"no_update\";a:8:{s:19:\"akismet/akismet.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:21:\"w.org/plugins/akismet\";s:4:\"slug\";s:7:\"akismet\";s:6:\"plugin\";s:19:\"akismet/akismet.php\";s:11:\"new_version\";s:5:\"4.1.2\";s:3:\"url\";s:38:\"https://wordpress.org/plugins/akismet/\";s:7:\"package\";s:56:\"https://downloads.wordpress.org/plugin/akismet.4.1.2.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:59:\"https://ps.w.org/akismet/assets/icon-256x256.png?rev=969272\";s:2:\"1x\";s:59:\"https://ps.w.org/akismet/assets/icon-128x128.png?rev=969272\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:61:\"https://ps.w.org/akismet/assets/banner-772x250.jpg?rev=479904\";}s:11:\"banners_rtl\";a:0:{}}s:51:\"all-in-one-wp-security-and-firewall/wp-security.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:49:\"w.org/plugins/all-in-one-wp-security-and-firewall\";s:4:\"slug\";s:35:\"all-in-one-wp-security-and-firewall\";s:6:\"plugin\";s:51:\"all-in-one-wp-security-and-firewall/wp-security.php\";s:11:\"new_version\";s:5:\"4.4.0\";s:3:\"url\";s:66:\"https://wordpress.org/plugins/all-in-one-wp-security-and-firewall/\";s:7:\"package\";s:78:\"https://downloads.wordpress.org/plugin/all-in-one-wp-security-and-firewall.zip\";s:5:\"icons\";a:1:{s:2:\"1x\";s:88:\"https://ps.w.org/all-in-one-wp-security-and-firewall/assets/icon-128x128.png?rev=1232826\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:91:\"https://ps.w.org/all-in-one-wp-security-and-firewall/assets/banner-1544x500.png?rev=1914011\";s:2:\"1x\";s:90:\"https://ps.w.org/all-in-one-wp-security-and-firewall/assets/banner-772x250.png?rev=1914013\";}s:11:\"banners_rtl\";a:0:{}}s:36:\"contact-form-7/wp-contact-form-7.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:28:\"w.org/plugins/contact-form-7\";s:4:\"slug\";s:14:\"contact-form-7\";s:6:\"plugin\";s:36:\"contact-form-7/wp-contact-form-7.php\";s:11:\"new_version\";s:5:\"5.1.4\";s:3:\"url\";s:45:\"https://wordpress.org/plugins/contact-form-7/\";s:7:\"package\";s:63:\"https://downloads.wordpress.org/plugin/contact-form-7.5.1.4.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:66:\"https://ps.w.org/contact-form-7/assets/icon-256x256.png?rev=984007\";s:2:\"1x\";s:66:\"https://ps.w.org/contact-form-7/assets/icon-128x128.png?rev=984007\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:69:\"https://ps.w.org/contact-form-7/assets/banner-1544x500.png?rev=860901\";s:2:\"1x\";s:68:\"https://ps.w.org/contact-form-7/assets/banner-772x250.png?rev=880427\";}s:11:\"banners_rtl\";a:0:{}}s:9:\"hello.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:25:\"w.org/plugins/hello-dolly\";s:4:\"slug\";s:11:\"hello-dolly\";s:6:\"plugin\";s:9:\"hello.php\";s:11:\"new_version\";s:5:\"1.7.2\";s:3:\"url\";s:42:\"https://wordpress.org/plugins/hello-dolly/\";s:7:\"package\";s:60:\"https://downloads.wordpress.org/plugin/hello-dolly.1.7.2.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:64:\"https://ps.w.org/hello-dolly/assets/icon-256x256.jpg?rev=2052855\";s:2:\"1x\";s:64:\"https://ps.w.org/hello-dolly/assets/icon-128x128.jpg?rev=2052855\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:66:\"https://ps.w.org/hello-dolly/assets/banner-772x250.jpg?rev=2052855\";}s:11:\"banners_rtl\";a:0:{}}s:21:\"meta-box/meta-box.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:22:\"w.org/plugins/meta-box\";s:4:\"slug\";s:8:\"meta-box\";s:6:\"plugin\";s:21:\"meta-box/meta-box.php\";s:11:\"new_version\";s:5:\"5.1.2\";s:3:\"url\";s:39:\"https://wordpress.org/plugins/meta-box/\";s:7:\"package\";s:57:\"https://downloads.wordpress.org/plugin/meta-box.5.1.2.zip\";s:5:\"icons\";a:1:{s:2:\"1x\";s:61:\"https://ps.w.org/meta-box/assets/icon-128x128.png?rev=1100915\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:63:\"https://ps.w.org/meta-box/assets/banner-772x250.png?rev=1929588\";}s:11:\"banners_rtl\";a:0:{}}s:37:\"post-types-order/post-types-order.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:30:\"w.org/plugins/post-types-order\";s:4:\"slug\";s:16:\"post-types-order\";s:6:\"plugin\";s:37:\"post-types-order/post-types-order.php\";s:11:\"new_version\";s:7:\"1.9.4.1\";s:3:\"url\";s:47:\"https://wordpress.org/plugins/post-types-order/\";s:7:\"package\";s:67:\"https://downloads.wordpress.org/plugin/post-types-order.1.9.4.1.zip\";s:5:\"icons\";a:1:{s:2:\"1x\";s:69:\"https://ps.w.org/post-types-order/assets/icon-128x128.png?rev=1226428\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:72:\"https://ps.w.org/post-types-order/assets/banner-1544x500.png?rev=1675574\";s:2:\"1x\";s:71:\"https://ps.w.org/post-types-order/assets/banner-772x250.png?rev=1429949\";}s:11:\"banners_rtl\";a:0:{}}s:35:\"redux-framework/redux-framework.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:29:\"w.org/plugins/redux-framework\";s:4:\"slug\";s:15:\"redux-framework\";s:6:\"plugin\";s:35:\"redux-framework/redux-framework.php\";s:11:\"new_version\";s:6:\"3.6.15\";s:3:\"url\";s:46:\"https://wordpress.org/plugins/redux-framework/\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/plugin/redux-framework.3.6.15.zip\";s:5:\"icons\";a:3:{s:2:\"2x\";s:67:\"https://ps.w.org/redux-framework/assets/icon-256x256.png?rev=995554\";s:2:\"1x\";s:59:\"https://ps.w.org/redux-framework/assets/icon.svg?rev=995554\";s:3:\"svg\";s:59:\"https://ps.w.org/redux-framework/assets/icon.svg?rev=995554\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:69:\"https://ps.w.org/redux-framework/assets/banner-772x250.png?rev=793165\";}s:11:\"banners_rtl\";a:0:{}}s:21:\"safe-svg/safe-svg.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:22:\"w.org/plugins/safe-svg\";s:4:\"slug\";s:8:\"safe-svg\";s:6:\"plugin\";s:21:\"safe-svg/safe-svg.php\";s:11:\"new_version\";s:5:\"1.9.4\";s:3:\"url\";s:39:\"https://wordpress.org/plugins/safe-svg/\";s:7:\"package\";s:57:\"https://downloads.wordpress.org/plugin/safe-svg.1.9.4.zip\";s:5:\"icons\";a:3:{s:2:\"2x\";s:61:\"https://ps.w.org/safe-svg/assets/icon-256x256.png?rev=1706191\";s:2:\"1x\";s:53:\"https://ps.w.org/safe-svg/assets/icon.svg?rev=1706191\";s:3:\"svg\";s:53:\"https://ps.w.org/safe-svg/assets/icon.svg?rev=1706191\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:64:\"https://ps.w.org/safe-svg/assets/banner-1544x500.png?rev=1706191\";s:2:\"1x\";s:63:\"https://ps.w.org/safe-svg/assets/banner-772x250.png?rev=1706191\";}s:11:\"banners_rtl\";a:0:{}}}}', 'no');

-- --------------------------------------------------------

--
-- Estrutura da tabela `wp_postmeta`
--

DROP TABLE IF EXISTS `wp_postmeta`;
CREATE TABLE IF NOT EXISTS `wp_postmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `post_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`meta_id`),
  KEY `post_id` (`post_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=MyISAM AUTO_INCREMENT=221 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `wp_postmeta`
--

INSERT INTO `wp_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(1, 2, '_wp_page_template', 'default'),
(2, 3, '_wp_page_template', 'default'),
(3, 5, '_form', '[text* nome id:nome class:nome placeholder \"Nome\"][email* email id:email class:email placeholder \"Email\"][number telefone id:telefone class:telefone placeholder \"Telefone\"][text* empresa id:empresa class:empresa placeholder \"Empresa\"][select* fusohorario id:fusohorario class:fusohorario \"Fuso Horário\" \"Fuso Horário\"][select* MotivodoContato id:MotivodoContato class:MotivodoContato \"Motivo do Contato\" \"Motivo do Contato\"][submit id:enviar class:enviar \"Enviar\"]'),
(4, 5, '_mail', 'a:9:{s:6:\"active\";b:1;s:7:\"subject\";s:26:\"We Payout \"[your-subject]\"\";s:6:\"sender\";s:38:\"We Payout <carolinoamandacs@gmail.com>\";s:9:\"recipient\";s:26:\"carolinoamandacs@gmail.com\";s:4:\"body\";s:187:\"From: [your-name] <[your-email]>\nSubject: [your-subject]\n\nMessage Body:\n[your-message]\n\n-- \nThis e-mail was sent from a contact form on We Payout (http://localhost/projetos/wepayout_site)\";s:18:\"additional_headers\";s:22:\"Reply-To: [your-email]\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";b:0;s:13:\"exclude_blank\";b:0;}'),
(5, 5, '_mail_2', 'a:9:{s:6:\"active\";b:0;s:7:\"subject\";s:26:\"We Payout \"[your-subject]\"\";s:6:\"sender\";s:38:\"We Payout <carolinoamandacs@gmail.com>\";s:9:\"recipient\";s:12:\"[your-email]\";s:4:\"body\";s:129:\"Message Body:\n[your-message]\n\n-- \nThis e-mail was sent from a contact form on We Payout (http://localhost/projetos/wepayout_site)\";s:18:\"additional_headers\";s:36:\"Reply-To: carolinoamandacs@gmail.com\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";b:0;s:13:\"exclude_blank\";b:0;}'),
(6, 5, '_messages', 'a:23:{s:12:\"mail_sent_ok\";s:45:\"Thank you for your message. It has been sent.\";s:12:\"mail_sent_ng\";s:71:\"There was an error trying to send your message. Please try again later.\";s:16:\"validation_error\";s:61:\"One or more fields have an error. Please check and try again.\";s:4:\"spam\";s:71:\"There was an error trying to send your message. Please try again later.\";s:12:\"accept_terms\";s:69:\"You must accept the terms and conditions before sending your message.\";s:16:\"invalid_required\";s:22:\"The field is required.\";s:16:\"invalid_too_long\";s:22:\"The field is too long.\";s:17:\"invalid_too_short\";s:23:\"The field is too short.\";s:12:\"invalid_date\";s:29:\"The date format is incorrect.\";s:14:\"date_too_early\";s:44:\"The date is before the earliest one allowed.\";s:13:\"date_too_late\";s:41:\"The date is after the latest one allowed.\";s:13:\"upload_failed\";s:46:\"There was an unknown error uploading the file.\";s:24:\"upload_file_type_invalid\";s:49:\"You are not allowed to upload files of this type.\";s:21:\"upload_file_too_large\";s:20:\"The file is too big.\";s:23:\"upload_failed_php_error\";s:38:\"There was an error uploading the file.\";s:14:\"invalid_number\";s:29:\"The number format is invalid.\";s:16:\"number_too_small\";s:47:\"The number is smaller than the minimum allowed.\";s:16:\"number_too_large\";s:46:\"The number is larger than the maximum allowed.\";s:23:\"quiz_answer_not_correct\";s:36:\"The answer to the quiz is incorrect.\";s:17:\"captcha_not_match\";s:31:\"Your entered code is incorrect.\";s:13:\"invalid_email\";s:38:\"The e-mail address entered is invalid.\";s:11:\"invalid_url\";s:19:\"The URL is invalid.\";s:11:\"invalid_tel\";s:32:\"The telephone number is invalid.\";}'),
(7, 5, '_additional_settings', ''),
(8, 5, '_locale', 'pt_BR'),
(9, 6, '_edit_lock', '1567540004:1'),
(10, 6, '_wp_page_template', 'pages/inicial.php'),
(11, 8, '_menu_item_type', 'post_type'),
(12, 8, '_menu_item_menu_item_parent', '0'),
(13, 8, '_menu_item_object_id', '6'),
(14, 8, '_menu_item_object', 'page'),
(15, 8, '_menu_item_target', ''),
(16, 8, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(17, 8, '_menu_item_xfn', ''),
(18, 8, '_menu_item_url', ''),
(19, 8, '_menu_item_orphaned', '1567540256'),
(20, 9, '_menu_item_type', 'post_type'),
(21, 9, '_menu_item_menu_item_parent', '0'),
(22, 9, '_menu_item_object_id', '6'),
(23, 9, '_menu_item_object', 'page'),
(24, 9, '_menu_item_target', ''),
(25, 9, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(26, 9, '_menu_item_xfn', ''),
(27, 9, '_menu_item_url', ''),
(28, 9, '_menu_item_orphaned', '1567540256'),
(29, 10, '_menu_item_type', 'post_type'),
(30, 10, '_menu_item_menu_item_parent', '0'),
(31, 10, '_menu_item_object_id', '2'),
(32, 10, '_menu_item_object', 'page'),
(33, 10, '_menu_item_target', ''),
(34, 10, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(35, 10, '_menu_item_xfn', ''),
(36, 10, '_menu_item_url', ''),
(37, 10, '_menu_item_orphaned', '1567540256'),
(38, 11, '_menu_item_type', 'post_type'),
(39, 11, '_menu_item_menu_item_parent', '0'),
(40, 11, '_menu_item_object_id', '6'),
(41, 11, '_menu_item_object', 'page'),
(42, 11, '_menu_item_target', ''),
(43, 11, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(44, 11, '_menu_item_xfn', ''),
(45, 11, '_menu_item_url', ''),
(46, 11, '_menu_item_orphaned', '1567540291'),
(47, 12, '_menu_item_type', 'post_type'),
(48, 12, '_menu_item_menu_item_parent', '0'),
(49, 12, '_menu_item_object_id', '6'),
(50, 12, '_menu_item_object', 'page'),
(51, 12, '_menu_item_target', ''),
(52, 12, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(53, 12, '_menu_item_xfn', ''),
(54, 12, '_menu_item_url', ''),
(55, 12, '_menu_item_orphaned', '1567540291'),
(56, 13, '_menu_item_type', 'post_type'),
(57, 13, '_menu_item_menu_item_parent', '0'),
(58, 13, '_menu_item_object_id', '2'),
(59, 13, '_menu_item_object', 'page'),
(60, 13, '_menu_item_target', ''),
(61, 13, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(62, 13, '_menu_item_xfn', ''),
(63, 13, '_menu_item_url', ''),
(64, 13, '_menu_item_orphaned', '1567540291'),
(65, 14, '_menu_item_type', 'custom'),
(66, 14, '_menu_item_menu_item_parent', '0'),
(67, 14, '_menu_item_object_id', '14'),
(68, 14, '_menu_item_object', 'custom'),
(69, 14, '_menu_item_target', ''),
(70, 14, '_menu_item_classes', 'a:1:{i:0;s:10:\"scrollTela\";}'),
(71, 14, '_menu_item_xfn', ''),
(72, 14, '_menu_item_url', '#servicos'),
(113, 20, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1920;s:6:\"height\";i:631;s:4:\"file\";s:21:\"2019/09/Header-BG.png\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:21:\"Header-BG-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:20:\"Header-BG-300x99.png\";s:5:\"width\";i:300;s:6:\"height\";i:99;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:21:\"Header-BG-768x252.png\";s:5:\"width\";i:768;s:6:\"height\";i:252;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:22:\"Header-BG-1024x337.png\";s:5:\"width\";i:1024;s:6:\"height\";i:337;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(74, 15, '_menu_item_type', 'custom'),
(75, 15, '_menu_item_menu_item_parent', '0'),
(76, 15, '_menu_item_object_id', '15'),
(77, 15, '_menu_item_object', 'custom'),
(78, 15, '_menu_item_target', ''),
(79, 15, '_menu_item_classes', 'a:1:{i:0;s:10:\"scrollTela\";}'),
(80, 15, '_menu_item_xfn', ''),
(81, 15, '_menu_item_url', '#solucoes'),
(83, 16, '_menu_item_type', 'custom'),
(84, 16, '_menu_item_menu_item_parent', '0'),
(85, 16, '_menu_item_object_id', '16'),
(86, 16, '_menu_item_object', 'custom'),
(87, 16, '_menu_item_target', ''),
(88, 16, '_menu_item_classes', 'a:1:{i:0;s:10:\"scrollTela\";}'),
(89, 16, '_menu_item_xfn', ''),
(90, 16, '_menu_item_url', '#comofunciona'),
(112, 20, '_wp_attached_file', '2019/09/Header-BG.png'),
(92, 17, '_menu_item_type', 'custom'),
(93, 17, '_menu_item_menu_item_parent', '0'),
(94, 17, '_menu_item_object_id', '17'),
(95, 17, '_menu_item_object', 'custom'),
(96, 17, '_menu_item_target', ''),
(97, 17, '_menu_item_classes', 'a:1:{i:0;s:10:\"scrollTela\";}'),
(98, 17, '_menu_item_xfn', ''),
(99, 17, '_menu_item_url', '#quemSomos'),
(111, 19, '_edit_lock', '1567612478:1'),
(101, 18, '_menu_item_type', 'custom'),
(102, 18, '_menu_item_menu_item_parent', '0'),
(103, 18, '_menu_item_object_id', '18'),
(104, 18, '_menu_item_object', 'custom'),
(105, 18, '_menu_item_target', ''),
(106, 18, '_menu_item_classes', 'a:1:{i:0;s:10:\"scrollTela\";}'),
(107, 18, '_menu_item_xfn', ''),
(108, 18, '_menu_item_url', '#contato'),
(110, 19, '_edit_last', '1'),
(114, 19, '_thumbnail_id', '20'),
(115, 19, '_wp_old_slug', 'destaque1'),
(116, 19, 'WeLayout_link_destaque', '#contato'),
(117, 21, '_wp_attached_file', '2019/09/logo.png'),
(118, 21, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:139;s:6:\"height\";i:57;s:4:\"file\";s:16:\"2019/09/logo.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(119, 22, '_wp_attached_file', '2019/09/wepayout.png'),
(120, 22, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:139;s:6:\"height\";i:57;s:4:\"file\";s:20:\"2019/09/wepayout.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(121, 23, '_wp_attached_file', '2019/09/s1.png'),
(122, 23, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:45;s:6:\"height\";i:50;s:4:\"file\";s:14:\"2019/09/s1.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(123, 24, '_wp_attached_file', '2019/09/image10.png'),
(124, 24, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:530;s:6:\"height\";i:703;s:4:\"file\";s:19:\"2019/09/image10.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:19:\"image10-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:19:\"image10-226x300.png\";s:5:\"width\";i:226;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(125, 25, '_wp_attached_file', '2019/09/building.png'),
(126, 25, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:108;s:6:\"height\";i:108;s:4:\"file\";s:20:\"2019/09/building.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(127, 26, '_wp_attached_file', '2019/09/image28.png'),
(128, 26, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:66;s:6:\"height\";i:76;s:4:\"file\";s:19:\"2019/09/image28.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(129, 27, '_wp_attached_file', '2019/09/image25.png'),
(130, 27, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:80;s:6:\"height\";i:108;s:4:\"file\";s:19:\"2019/09/image25.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(131, 28, '_wp_attached_file', '2019/09/image58.png'),
(132, 28, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:365;s:6:\"height\";i:451;s:4:\"file\";s:19:\"2019/09/image58.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:19:\"image58-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:19:\"image58-243x300.png\";s:5:\"width\";i:243;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(133, 29, '_wp_attached_file', '2019/09/Group.png'),
(134, 29, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:564;s:6:\"height\";i:289;s:4:\"file\";s:17:\"2019/09/Group.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:17:\"Group-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:17:\"Group-300x154.png\";s:5:\"width\";i:300;s:6:\"height\";i:154;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(135, 30, '_wp_attached_file', '2019/09/14610.png'),
(136, 30, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1629;s:6:\"height\";i:540;s:4:\"file\";s:17:\"2019/09/14610.png\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:17:\"14610-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:16:\"14610-300x99.png\";s:5:\"width\";i:300;s:6:\"height\";i:99;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:17:\"14610-768x255.png\";s:5:\"width\";i:768;s:6:\"height\";i:255;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:18:\"14610-1024x339.png\";s:5:\"width\";i:1024;s:6:\"height\";i:339;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(137, 5, '_config_errors', 'a:1:{s:23:\"mail.additional_headers\";a:1:{i:0;a:2:{s:4:\"code\";i:102;s:4:\"args\";a:3:{s:7:\"message\";s:51:\"Invalid mailbox syntax is used in the %name% field.\";s:6:\"params\";a:1:{s:4:\"name\";s:8:\"Reply-To\";}s:4:\"link\";s:68:\"https://contactform7.com/configuration-errors/invalid-mailbox-syntax\";}}}}'),
(138, 35, '_edit_last', '1'),
(139, 35, '_edit_lock', '1567610048:1'),
(140, 36, '_wp_attached_file', '2019/09/Component.png'),
(141, 36, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:64;s:6:\"height\";i:63;s:4:\"file\";s:21:\"2019/09/Component.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(142, 35, '_thumbnail_id', '36'),
(143, 35, 'WeLayout_descricao_servico', 'Os pagamentos são processados em D0. Todo o processamento é automatizado garantindo rapidez e segurança, porém sem perder o foco em cuidar de cada pagamento com o mesmo zêlo.'),
(144, 37, '_edit_last', '1'),
(145, 37, '_edit_lock', '1567610070:1'),
(146, 38, '_wp_attached_file', '2019/09/Component-2.png'),
(147, 38, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:66;s:6:\"height\";i:66;s:4:\"file\";s:23:\"2019/09/Component-2.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(148, 37, '_thumbnail_id', '38'),
(149, 37, 'WeLayout_descricao_servico', 'Aproveite da automatização da WePayout e reduza os riscos de fraude interna, riscos causados por erros operacionais garantindo egiciência no processo de pagamentos.'),
(150, 39, '_edit_last', '1'),
(151, 39, '_edit_lock', '1567610090:1'),
(152, 40, '_wp_attached_file', '2019/09/e4.png'),
(153, 40, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:49;s:6:\"height\";i:50;s:4:\"file\";s:14:\"2019/09/e4.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(154, 39, '_thumbnail_id', '40'),
(155, 39, 'WeLayout_descricao_servico', 'Todos os pagamentos são conciliados. Faça o download dos relatórios diretamente no painel da WePayout. As informações aparecem em tempo real.'),
(156, 41, '_edit_last', '1'),
(157, 41, '_edit_lock', '1567610108:1'),
(158, 42, '_wp_attached_file', '2019/09/Component-4.png'),
(159, 42, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:65;s:6:\"height\";i:63;s:4:\"file\";s:23:\"2019/09/Component-4.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(160, 41, '_thumbnail_id', '42'),
(161, 41, 'WeLayout_descricao_servico', 'Com a validação de dados bancários conseguimos promover mais de 90% em pagamentos aprovados.'),
(162, 43, '_edit_last', '1'),
(163, 43, '_edit_lock', '1567610132:1'),
(164, 44, '_wp_attached_file', '2019/09/Component-5.png'),
(165, 44, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:66;s:6:\"height\";i:69;s:4:\"file\";s:23:\"2019/09/Component-5.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(166, 43, '_thumbnail_id', '44'),
(167, 43, 'WeLayout_descricao_servico', 'Comprovantes de pagamentos disponiveis quando e onde você quiser'),
(168, 45, '_edit_last', '1'),
(169, 45, '_edit_lock', '1567610181:1'),
(170, 46, '_wp_attached_file', '2019/09/Component-6.png'),
(171, 46, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:69;s:6:\"height\";i:69;s:4:\"file\";s:23:\"2019/09/Component-6.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(172, 45, '_thumbnail_id', '46'),
(173, 45, 'WeLayout_descricao_servico', 'Somos pessoas por trás da empresa. Nosso foco é prestar suporte sem enrolação. Resolvemos os problemas.'),
(174, 48, '_edit_last', '1'),
(175, 48, '_edit_lock', '1567610268:1'),
(176, 48, 'WeLayout_problemas_servico', 'Integrações com vários bancos. \r\nA WePayout esta integrada com os maiores\r\nbancos do Brasil.'),
(177, 49, '_edit_last', '1'),
(178, 49, '_edit_lock', '1567610280:1'),
(179, 49, 'WeLayout_problemas_servico', 'Todo o seu processo operacional de\r\npagamentos. Pagamos seus fornecedores,\r\nfuncionários, quem você precisar e onde eles\r\nprecisarem.'),
(180, 50, '_edit_last', '1'),
(181, 50, '_edit_lock', '1567610291:1'),
(182, 50, 'WeLayout_problemas_servico', 'As dificuldades técnicas e burocráticas.\r\nDesburocratizamos e facilitamos o processo\r\npara você.'),
(183, 51, '_edit_last', '1'),
(184, 51, '_edit_lock', '1567610301:1'),
(185, 51, 'WeLayout_problemas_servico', 'O risco financeiro operacional e diminua o risco\r\nfraude interna.'),
(186, 52, '_edit_last', '1'),
(187, 52, '_edit_lock', '1567610364:1'),
(188, 52, 'WeLayout_problemas_servico', 'Reduza custos nas transferências ganhando\r\nvelocidade, eficiência e mais tempo para você \r\nfocar no seu core business.'),
(189, 56, '_edit_last', '1'),
(190, 56, '_edit_lock', '1567610650:1'),
(191, 57, '_wp_attached_file', '2019/09/s1-1.png'),
(192, 57, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:45;s:6:\"height\";i:50;s:4:\"file\";s:16:\"2019/09/s1-1.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(193, 56, '_thumbnail_id', '57'),
(194, 56, 'WeLayout_pagamento_servico', 'Integração simples,\r\nrápida e segura'),
(195, 58, '_edit_last', '1'),
(196, 58, '_edit_lock', '1567610673:1'),
(197, 59, '_wp_attached_file', '2019/09/s2.png'),
(198, 59, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:49;s:6:\"height\";i:50;s:4:\"file\";s:14:\"2019/09/s2.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(199, 58, '_thumbnail_id', '59'),
(200, 58, 'WeLayout_pagamento_servico', 'Simplesmente envie uma \r\nTED para a WePayout'),
(201, 60, '_edit_last', '1'),
(202, 60, '_edit_lock', '1567612194:1'),
(203, 61, '_wp_attached_file', '2019/09/Component-5-1.png'),
(204, 61, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:66;s:6:\"height\";i:69;s:4:\"file\";s:25:\"2019/09/Component-5-1.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(205, 60, '_thumbnail_id', '66'),
(206, 60, 'WeLayout_pagamento_servico', 'Envie o lote de\r\npagamentos via API ou\r\nfaça o upload na interface'),
(207, 62, '_edit_last', '1'),
(208, 62, '_edit_lock', '1567610721:1'),
(209, 63, '_wp_attached_file', '2019/09/e4-1.png'),
(210, 63, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:49;s:6:\"height\";i:50;s:4:\"file\";s:16:\"2019/09/e4-1.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(211, 62, '_thumbnail_id', '63'),
(212, 62, 'WeLayout_pagamento_servico', 'Pagamentos\r\nprocessados, conciliados\r\ne com comprovantes'),
(213, 64, '_edit_last', '1'),
(214, 64, '_edit_lock', '1567612122:1'),
(215, 65, '_wp_attached_file', '2019/09/s5.png'),
(216, 65, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:40;s:6:\"height\";i:27;s:4:\"file\";s:14:\"2019/09/s5.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(217, 64, '_thumbnail_id', '65'),
(218, 64, 'WeLayout_pagamento_servico', 'Fundos disponíveis\r\nna conta do beneficiário\r\nfinal'),
(219, 66, '_wp_attached_file', '2019/09/s3.png'),
(220, 66, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:44;s:6:\"height\";i:50;s:4:\"file\";s:14:\"2019/09/s3.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');

-- --------------------------------------------------------

--
-- Estrutura da tabela `wp_posts`
--

DROP TABLE IF EXISTS `wp_posts`;
CREATE TABLE IF NOT EXISTS `wp_posts` (
  `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `post_author` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_title` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_excerpt` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `pinged` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `guid` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `post_name` (`post_name`(191)),
  KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  KEY `post_parent` (`post_parent`),
  KEY `post_author` (`post_author`)
) ENGINE=MyISAM AUTO_INCREMENT=67 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `wp_posts`
--

INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(1, 1, '2019-09-03 15:30:16', '2019-09-03 18:30:16', '<!-- wp:paragraph -->\n<p>Boas-vindas ao WordPress. Esse é o seu primeiro post. Edite-o ou exclua-o, e então comece a escrever!</p>\n<!-- /wp:paragraph -->', 'Olá, mundo!', '', 'publish', 'open', 'open', '', 'ola-mundo', '', '', '2019-09-03 15:30:16', '2019-09-03 18:30:16', '', 0, 'http://localhost/projetos/wepayout_site/?p=1', 0, 'post', '', 1),
(2, 1, '2019-09-03 15:30:16', '2019-09-03 18:30:16', '<!-- wp:paragraph -->\n<p>Esta é uma página de exemplo. É diferente de um post no blog porque ela permanecerá em um lugar e aparecerá na navegação do seu site na maioria dos temas. Muitas pessoas começam com uma página que as apresenta a possíveis visitantes do site. Ela pode dizer algo assim:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>Olá! Eu sou um mensageiro de bicicleta durante o dia, ator aspirante à noite, e este é o meu site. Eu moro em São Paulo, tenho um grande cachorro chamado Rex e gosto de tomar caipirinha (e banhos de chuva).</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>...ou alguma coisa assim:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>A Companhia de Miniaturas XYZ foi fundada em 1971, e desde então tem fornecido miniaturas de qualidade ao público. Localizada na cidade de Itu, a XYZ emprega mais de 2.000 pessoas e faz coisas grandiosas para a comunidade da cidade.</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>Como um novo usuário do WordPress, você deveria ir ao <a href=\"http://localhost/projetos/wepayout_site/wp-admin/\">painel</a> para excluir essa página e criar novas páginas para o seu conteúdo. Divirta-se!</p>\n<!-- /wp:paragraph -->', 'Página de exemplo', '', 'publish', 'closed', 'open', '', 'pagina-exemplo', '', '', '2019-09-03 15:30:16', '2019-09-03 18:30:16', '', 0, 'http://localhost/projetos/wepayout_site/?page_id=2', 0, 'page', '', 0),
(3, 1, '2019-09-03 15:30:16', '2019-09-03 18:30:16', '<!-- wp:heading --><h2>Quem somos</h2><!-- /wp:heading --><!-- wp:paragraph --><p>O endereço do nosso site é: http://localhost/projetos/wepayout_site.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Quais dados pessoais coletamos e porque</h2><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Comentários</h3><!-- /wp:heading --><!-- wp:paragraph --><p>Quando os visitantes deixam comentários no site, coletamos os dados mostrados no formulário de comentários, além do endereço de IP e de dados do navegador do visitante, para auxiliar na detecção de spam.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>Uma sequência anonimizada de caracteres criada a partir do seu e-mail (também chamada de hash) poderá ser enviada para o Gravatar para verificar se você usa o serviço. A política de privacidade do Gravatar está disponível aqui: https://automattic.com/privacy/. Depois da aprovação do seu comentário, a foto do seu perfil fica visível publicamente junto de seu comentário.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Mídia</h3><!-- /wp:heading --><!-- wp:paragraph --><p>Se você envia imagens para o site, evite enviar as que contenham dados de localização incorporados (EXIF GPS). Visitantes podem baixar estas imagens do site e extrair delas seus dados de localização.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Formulários de contato</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Cookies</h3><!-- /wp:heading --><!-- wp:paragraph --><p>Ao deixar um comentário no site, você poderá optar por salvar seu nome, e-mail e site nos cookies. Isso visa seu conforto, assim você não precisará preencher seus  dados novamente quando fizer outro comentário. Estes cookies duram um ano.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>Se você tem uma conta e acessa este site, um cookie temporário será criado para determinar se seu navegador aceita cookies. Ele não contém nenhum dado pessoal e será descartado quando você fechar seu navegador.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>Quando você acessa sua conta no site, também criamos vários cookies para salvar os dados da sua conta e suas escolhas de exibição de tela. Cookies de login são mantidos por dois dias e cookies de opções de tela por um ano. Se você selecionar &quot;Lembrar-me&quot;, seu acesso será mantido por duas semanas. Se você se desconectar da sua conta, os cookies de login serão removidos.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>Se você editar ou publicar um artigo, um cookie adicional será salvo no seu navegador. Este cookie não inclui nenhum dado pessoal e simplesmente indica o ID do post referente ao artigo que você acabou de editar. Ele expira depois de 1 dia.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Mídia incorporada de outros sites</h3><!-- /wp:heading --><!-- wp:paragraph --><p>Artigos neste site podem incluir conteúdo incorporado como, por exemplo, vídeos, imagens, artigos, etc. Conteúdos incorporados de outros sites se comportam exatamente da mesma forma como se o visitante estivesse visitando o outro site.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>Estes sites podem coletar dados sobre você, usar cookies, incorporar rastreamento adicional de terceiros e monitorar sua interação com este conteúdo incorporado, incluindo sua interação com o conteúdo incorporado se você tem uma conta e está conectado com o site.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Análises</h3><!-- /wp:heading --><!-- wp:heading --><h2>Com quem partilhamos seus dados</h2><!-- /wp:heading --><!-- wp:heading --><h2>Por quanto tempo mantemos os seus dados</h2><!-- /wp:heading --><!-- wp:paragraph --><p>Se você deixar um comentário, o comentário e os seus metadados são conservados indefinidamente. Fazemos isso para que seja possível reconhecer e aprovar automaticamente qualquer comentário posterior ao invés de retê-lo para moderação.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>Para usuários que se registram no nosso site (se houver), também guardamos as informações pessoais que fornecem no seu perfil de usuário. Todos os usuários podem ver, editar ou excluir suas informações pessoais a qualquer momento (só não é possível alterar o seu username). Os administradores de sites também podem ver e editar estas informações.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Quais os seus direitos sobre seus dados</h2><!-- /wp:heading --><!-- wp:paragraph --><p>Se você tiver uma conta neste site ou se tiver deixado comentários, pode solicitar um arquivo exportado dos dados pessoais que mantemos sobre você, inclusive quaisquer dados que nos tenha fornecido. Também pode solicitar que removamos qualquer dado pessoal que mantemos sobre você. Isto não inclui nenhuns dados que somos obrigados a manter para propósitos administrativos, legais ou de segurança.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Para onde enviamos seus dados</h2><!-- /wp:heading --><!-- wp:paragraph --><p>Comentários de visitantes podem ser marcados por um serviço automático de detecção de spam.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Suas informações de contato</h2><!-- /wp:heading --><!-- wp:heading --><h2>Informações adicionais</h2><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Como protegemos seus dados</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Quais são nossos procedimentos contra violação de dados</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>De quais terceiros nós recebemos dados</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Quais tomadas de decisão ou análises de perfil automatizadas fazemos com os dados de usuários</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Requisitos obrigatórios de divulgação para sua categoria profissional</h3><!-- /wp:heading -->', 'Política de privacidade', '', 'draft', 'closed', 'open', '', 'politica-de-privacidade', '', '', '2019-09-03 15:30:16', '2019-09-03 18:30:16', '', 0, 'http://localhost/projetos/wepayout_site/?page_id=3', 0, 'page', '', 0),
(4, 1, '2019-09-03 15:30:33', '0000-00-00 00:00:00', '', 'Rascunho automático', '', 'auto-draft', 'open', 'open', '', '', '', '', '2019-09-03 15:30:33', '0000-00-00 00:00:00', '', 0, 'http://localhost/projetos/wepayout_site/?p=4', 0, 'post', '', 0),
(5, 1, '2019-09-03 15:34:54', '2019-09-03 18:34:54', '[text* nome id:nome class:nome placeholder \"Nome\"][email* email id:email class:email placeholder \"Email\"][number telefone id:telefone class:telefone placeholder \"Telefone\"][text* empresa id:empresa class:empresa placeholder \"Empresa\"][select* fusohorario id:fusohorario class:fusohorario \"Fuso Horário\" \"Fuso Horário\"][select* MotivodoContato id:MotivodoContato class:MotivodoContato \"Motivo do Contato\" \"Motivo do Contato\"][submit id:enviar class:enviar \"Enviar\"]\n1\nWe Payout \"[your-subject]\"\nWe Payout <carolinoamandacs@gmail.com>\ncarolinoamandacs@gmail.com\nFrom: [your-name] <[your-email]>\r\nSubject: [your-subject]\r\n\r\nMessage Body:\r\n[your-message]\r\n\r\n-- \r\nThis e-mail was sent from a contact form on We Payout (http://localhost/projetos/wepayout_site)\nReply-To: [your-email]\n\n\n\n\nWe Payout \"[your-subject]\"\nWe Payout <carolinoamandacs@gmail.com>\n[your-email]\nMessage Body:\r\n[your-message]\r\n\r\n-- \r\nThis e-mail was sent from a contact form on We Payout (http://localhost/projetos/wepayout_site)\nReply-To: carolinoamandacs@gmail.com\n\n\n\nThank you for your message. It has been sent.\nThere was an error trying to send your message. Please try again later.\nOne or more fields have an error. Please check and try again.\nThere was an error trying to send your message. Please try again later.\nYou must accept the terms and conditions before sending your message.\nThe field is required.\nThe field is too long.\nThe field is too short.\nThe date format is incorrect.\nThe date is before the earliest one allowed.\nThe date is after the latest one allowed.\nThere was an unknown error uploading the file.\nYou are not allowed to upload files of this type.\nThe file is too big.\nThere was an error uploading the file.\nThe number format is invalid.\nThe number is smaller than the minimum allowed.\nThe number is larger than the maximum allowed.\nThe answer to the quiz is incorrect.\nYour entered code is incorrect.\nThe e-mail address entered is invalid.\nThe URL is invalid.\nThe telephone number is invalid.', 'Contato', '', 'publish', 'closed', 'closed', '', 'contact-form-1', '', '', '2019-09-04 10:59:22', '2019-09-04 13:59:22', '', 0, 'http://localhost/projetos/wepayout_site/?post_type=wpcf7_contact_form&#038;p=5', 0, 'wpcf7_contact_form', '', 0),
(6, 1, '2019-09-03 16:48:57', '2019-09-03 19:48:57', '', 'Home', '', 'publish', 'closed', 'closed', '', 'home', '', '', '2019-09-03 16:48:57', '2019-09-03 19:48:57', '', 0, 'http://localhost/projetos/wepayout_site/?page_id=6', 0, 'page', '', 0),
(7, 1, '2019-09-03 16:48:57', '2019-09-03 19:48:57', '', 'Home', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2019-09-03 16:48:57', '2019-09-03 19:48:57', '', 6, 'http://localhost/projetos/wepayout_site/2019/09/03/6-revision-v1/', 0, 'revision', '', 0),
(8, 1, '2019-09-03 16:50:56', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2019-09-03 16:50:56', '0000-00-00 00:00:00', '', 0, 'http://localhost/projetos/wepayout_site/?p=8', 1, 'nav_menu_item', '', 0),
(9, 1, '2019-09-03 16:50:56', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2019-09-03 16:50:56', '0000-00-00 00:00:00', '', 0, 'http://localhost/projetos/wepayout_site/?p=9', 1, 'nav_menu_item', '', 0),
(10, 1, '2019-09-03 16:50:56', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2019-09-03 16:50:56', '0000-00-00 00:00:00', '', 0, 'http://localhost/projetos/wepayout_site/?p=10', 1, 'nav_menu_item', '', 0),
(11, 1, '2019-09-03 16:51:31', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2019-09-03 16:51:31', '0000-00-00 00:00:00', '', 0, 'http://localhost/projetos/wepayout_site/?p=11', 1, 'nav_menu_item', '', 0),
(12, 1, '2019-09-03 16:51:31', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2019-09-03 16:51:31', '0000-00-00 00:00:00', '', 0, 'http://localhost/projetos/wepayout_site/?p=12', 1, 'nav_menu_item', '', 0),
(13, 1, '2019-09-03 16:51:31', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2019-09-03 16:51:31', '0000-00-00 00:00:00', '', 0, 'http://localhost/projetos/wepayout_site/?p=13', 1, 'nav_menu_item', '', 0),
(14, 1, '2019-09-03 16:52:54', '2019-09-03 19:52:54', '', 'Serviços', '', 'publish', 'closed', 'closed', '', 'servicos', '', '', '2019-09-03 17:05:23', '2019-09-03 20:05:23', '', 0, 'http://localhost/projetos/wepayout_site/?p=14', 1, 'nav_menu_item', '', 0),
(15, 1, '2019-09-03 16:52:54', '2019-09-03 19:52:54', '', 'Soluções', '', 'publish', 'closed', 'closed', '', 'solucoes', '', '', '2019-09-03 17:05:23', '2019-09-03 20:05:23', '', 0, 'http://localhost/projetos/wepayout_site/?p=15', 2, 'nav_menu_item', '', 0),
(16, 1, '2019-09-03 16:52:54', '2019-09-03 19:52:54', '', 'Como Funciona?', '', 'publish', 'closed', 'closed', '', 'como-funciona', '', '', '2019-09-03 17:05:23', '2019-09-03 20:05:23', '', 0, 'http://localhost/projetos/wepayout_site/?p=16', 3, 'nav_menu_item', '', 0),
(17, 1, '2019-09-03 16:52:54', '2019-09-03 19:52:54', '', 'Quem Somos?', '', 'publish', 'closed', 'closed', '', 'quem-somos', '', '', '2019-09-03 17:05:23', '2019-09-03 20:05:23', '', 0, 'http://localhost/projetos/wepayout_site/?p=17', 4, 'nav_menu_item', '', 0),
(18, 1, '2019-09-03 16:52:54', '2019-09-03 19:52:54', '', 'Contato', '', 'publish', 'closed', 'closed', '', 'contato', '', '', '2019-09-03 17:05:23', '2019-09-03 20:05:23', '', 0, 'http://localhost/projetos/wepayout_site/?p=18', 5, 'nav_menu_item', '', 0),
(19, 1, '2019-09-03 16:54:54', '2019-09-03 19:54:54', 'Ganhe tempo e segurança contando com uma estrutura\r\nespecializada de pagamentos, sistemas, pessoas e processos\r\nautomatizados.', 'Sua solução tecnológica e customizavél de pagamentos a terceiros, fornecedores e <br>  estornos.', '', 'publish', 'closed', 'closed', '', 'sua-solucao-tecnologica-e-customizavel-de-pagamentos-a-terceiros-fornecedores-e-estornos', '', '', '2019-09-04 12:52:28', '2019-09-04 15:52:28', '', 0, 'http://localhost/projetos/wepayout_site/?post_type=destaque&#038;p=19', 0, 'destaque', '', 0),
(20, 1, '2019-09-03 16:54:50', '2019-09-03 19:54:50', '', 'Header-BG', '', 'inherit', 'open', 'closed', '', 'header-bg', '', '', '2019-09-03 16:54:50', '2019-09-03 19:54:50', '', 19, 'http://localhost/projetos/wepayout_site/wp-content/uploads/2019/09/Header-BG.png', 0, 'attachment', 'image/png', 0),
(21, 1, '2019-09-03 17:09:04', '2019-09-03 20:09:04', '', 'logo', '', 'inherit', 'open', 'closed', '', 'logo', '', '', '2019-09-03 17:09:04', '2019-09-03 20:09:04', '', 0, 'http://localhost/projetos/wepayout_site/wp-content/uploads/2019/09/logo.png', 0, 'attachment', 'image/png', 0),
(22, 1, '2019-09-03 17:12:13', '2019-09-03 20:12:13', '', 'wepayout', '', 'inherit', 'open', 'closed', '', 'wepayout', '', '', '2019-09-03 17:12:13', '2019-09-03 20:12:13', '', 0, 'http://localhost/projetos/wepayout_site/wp-content/uploads/2019/09/wepayout.png', 0, 'attachment', 'image/png', 0),
(23, 1, '2019-09-04 10:21:36', '2019-09-04 13:21:36', '', 's1', '', 'inherit', 'open', 'closed', '', 's1', '', '', '2019-09-04 10:21:36', '2019-09-04 13:21:36', '', 0, 'http://localhost/projetos/wepayout_site/wp-content/uploads/2019/09/s1.png', 0, 'attachment', 'image/png', 0),
(24, 1, '2019-09-04 10:26:40', '2019-09-04 13:26:40', '', 'image10', '', 'inherit', 'open', 'closed', '', 'image10', '', '', '2019-09-04 10:26:40', '2019-09-04 13:26:40', '', 0, 'http://localhost/projetos/wepayout_site/wp-content/uploads/2019/09/image10.png', 0, 'attachment', 'image/png', 0),
(25, 1, '2019-09-04 10:36:56', '2019-09-04 13:36:56', '', 'building', '', 'inherit', 'open', 'closed', '', 'building', '', '', '2019-09-04 10:36:56', '2019-09-04 13:36:56', '', 0, 'http://localhost/projetos/wepayout_site/wp-content/uploads/2019/09/building.png', 0, 'attachment', 'image/png', 0),
(26, 1, '2019-09-04 10:37:06', '2019-09-04 13:37:06', '', 'image28', '', 'inherit', 'open', 'closed', '', 'image28', '', '', '2019-09-04 10:37:06', '2019-09-04 13:37:06', '', 0, 'http://localhost/projetos/wepayout_site/wp-content/uploads/2019/09/image28.png', 0, 'attachment', 'image/png', 0),
(27, 1, '2019-09-04 10:37:17', '2019-09-04 13:37:17', '', 'image25', '', 'inherit', 'open', 'closed', '', 'image25', '', '', '2019-09-04 10:37:17', '2019-09-04 13:37:17', '', 0, 'http://localhost/projetos/wepayout_site/wp-content/uploads/2019/09/image25.png', 0, 'attachment', 'image/png', 0),
(28, 1, '2019-09-04 10:44:24', '2019-09-04 13:44:24', '', 'image58', '', 'inherit', 'open', 'closed', '', 'image58', '', '', '2019-09-04 10:44:24', '2019-09-04 13:44:24', '', 0, 'http://localhost/projetos/wepayout_site/wp-content/uploads/2019/09/image58.png', 0, 'attachment', 'image/png', 0),
(29, 1, '2019-09-04 10:48:22', '2019-09-04 13:48:22', '', 'Group', '', 'inherit', 'open', 'closed', '', 'group', '', '', '2019-09-04 10:48:22', '2019-09-04 13:48:22', '', 0, 'http://localhost/projetos/wepayout_site/wp-content/uploads/2019/09/Group.png', 0, 'attachment', 'image/png', 0),
(30, 1, '2019-09-04 10:52:24', '2019-09-04 13:52:24', '', '14610', '', 'inherit', 'open', 'closed', '', '14610', '', '', '2019-09-04 10:52:24', '2019-09-04 13:52:24', '', 0, 'http://localhost/projetos/wepayout_site/wp-content/uploads/2019/09/14610.png', 0, 'attachment', 'image/png', 0),
(31, 1, '2019-09-04 12:13:27', '0000-00-00 00:00:00', '', 'Rascunho automático', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2019-09-04 12:13:27', '0000-00-00 00:00:00', '', 0, 'http://localhost/projetos/wepayout_site/?post_type=servico&p=31', 0, 'servico', '', 0),
(32, 1, '2019-09-04 12:13:56', '0000-00-00 00:00:00', '', 'Rascunho automático', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2019-09-04 12:13:56', '0000-00-00 00:00:00', '', 0, 'http://localhost/projetos/wepayout_site/?post_type=servico&p=32', 0, 'servico', '', 0),
(33, 1, '2019-09-04 12:14:07', '0000-00-00 00:00:00', '', 'Rascunho automático', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2019-09-04 12:14:07', '0000-00-00 00:00:00', '', 0, 'http://localhost/projetos/wepayout_site/?post_type=servico&p=33', 0, 'servico', '', 0),
(34, 1, '2019-09-04 12:14:35', '0000-00-00 00:00:00', '', 'Rascunho automático', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2019-09-04 12:14:35', '0000-00-00 00:00:00', '', 0, 'http://localhost/projetos/wepayout_site/?post_type=servico&p=34', 0, 'servico', '', 0),
(35, 1, '2019-09-04 12:16:27', '2019-09-04 15:16:27', '', 'Agilidade', '', 'publish', 'closed', 'closed', '', 'agilidade', '', '', '2019-09-04 12:16:27', '2019-09-04 15:16:27', '', 0, 'http://localhost/projetos/wepayout_site/?post_type=servico&#038;p=35', 0, 'servico', '', 0),
(36, 1, '2019-09-04 12:16:24', '2019-09-04 15:16:24', '', 'Component', '', 'inherit', 'open', 'closed', '', 'component', '', '', '2019-09-04 12:16:24', '2019-09-04 15:16:24', '', 35, 'http://localhost/projetos/wepayout_site/wp-content/uploads/2019/09/Component.png', 0, 'attachment', 'image/png', 0),
(37, 1, '2019-09-04 12:16:50', '2019-09-04 15:16:50', '', 'Foco em qualidade e automação', '', 'publish', 'closed', 'closed', '', 'foco-em-qualidade-e-automacao', '', '', '2019-09-04 12:16:50', '2019-09-04 15:16:50', '', 0, 'http://localhost/projetos/wepayout_site/?post_type=servico&#038;p=37', 0, 'servico', '', 0),
(38, 1, '2019-09-04 12:16:47', '2019-09-04 15:16:47', '', 'Component 2', '', 'inherit', 'open', 'closed', '', 'component-2', '', '', '2019-09-04 12:16:47', '2019-09-04 15:16:47', '', 37, 'http://localhost/projetos/wepayout_site/wp-content/uploads/2019/09/Component-2.png', 0, 'attachment', 'image/png', 0),
(39, 1, '2019-09-04 12:17:11', '2019-09-04 15:17:11', '', 'Relatórios e Conciliação', '', 'publish', 'closed', 'closed', '', 'relatorios-e-conciliacao', '', '', '2019-09-04 12:17:11', '2019-09-04 15:17:11', '', 0, 'http://localhost/projetos/wepayout_site/?post_type=servico&#038;p=39', 0, 'servico', '', 0),
(40, 1, '2019-09-04 12:17:08', '2019-09-04 15:17:08', '', 'e4', '', 'inherit', 'open', 'closed', '', 'e4', '', '', '2019-09-04 12:17:08', '2019-09-04 15:17:08', '', 39, 'http://localhost/projetos/wepayout_site/wp-content/uploads/2019/09/e4.png', 0, 'attachment', 'image/png', 0),
(41, 1, '2019-09-04 12:17:29', '2019-09-04 15:17:29', '', 'Mais pagamentos aprovados', '', 'publish', 'closed', 'closed', '', 'mais-pagamentos-aprovados', '', '', '2019-09-04 12:17:29', '2019-09-04 15:17:29', '', 0, 'http://localhost/projetos/wepayout_site/?post_type=servico&#038;p=41', 0, 'servico', '', 0),
(42, 1, '2019-09-04 12:17:26', '2019-09-04 15:17:26', '', 'Component 4', '', 'inherit', 'open', 'closed', '', 'component-4', '', '', '2019-09-04 12:17:26', '2019-09-04 15:17:26', '', 41, 'http://localhost/projetos/wepayout_site/wp-content/uploads/2019/09/Component-4.png', 0, 'attachment', 'image/png', 0),
(43, 1, '2019-09-04 12:17:52', '2019-09-04 15:17:52', '', 'Comprovantes', '', 'publish', 'closed', 'closed', '', 'comprovantes', '', '', '2019-09-04 12:17:52', '2019-09-04 15:17:52', '', 0, 'http://localhost/projetos/wepayout_site/?post_type=servico&#038;p=43', 0, 'servico', '', 0),
(44, 1, '2019-09-04 12:17:50', '2019-09-04 15:17:50', '', 'Component 5', '', 'inherit', 'open', 'closed', '', 'component-5', '', '', '2019-09-04 12:17:50', '2019-09-04 15:17:50', '', 43, 'http://localhost/projetos/wepayout_site/wp-content/uploads/2019/09/Component-5.png', 0, 'attachment', 'image/png', 0),
(45, 1, '2019-09-04 12:18:15', '2019-09-04 15:18:15', '', 'Atendimento sem enrolação', '', 'publish', 'closed', 'closed', '', 'atendimento-sem-enrolacao', '', '', '2019-09-04 12:18:15', '2019-09-04 15:18:15', '', 0, 'http://localhost/projetos/wepayout_site/?post_type=servico&#038;p=45', 0, 'servico', '', 0),
(46, 1, '2019-09-04 12:18:13', '2019-09-04 15:18:13', '', 'Component 6', '', 'inherit', 'open', 'closed', '', 'component-6', '', '', '2019-09-04 12:18:13', '2019-09-04 15:18:13', '', 45, 'http://localhost/projetos/wepayout_site/wp-content/uploads/2019/09/Component-6.png', 0, 'attachment', 'image/png', 0),
(47, 1, '2019-09-04 12:18:55', '0000-00-00 00:00:00', '', 'Rascunho automático', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2019-09-04 12:18:55', '0000-00-00 00:00:00', '', 0, 'http://localhost/projetos/wepayout_site/?post_type=problema&p=47', 0, 'problema', '', 0),
(48, 1, '2019-09-04 12:20:08', '2019-09-04 15:20:08', '', 'Esqueça', '', 'publish', 'closed', 'closed', '', 'esqueca', '', '', '2019-09-04 12:20:08', '2019-09-04 15:20:08', '', 0, 'http://localhost/projetos/wepayout_site/?post_type=problema&#038;p=48', 0, 'problema', '', 0),
(49, 1, '2019-09-04 12:20:20', '2019-09-04 15:20:20', '', 'Simplifique', '', 'publish', 'closed', 'closed', '', 'simplifique', '', '', '2019-09-04 12:20:20', '2019-09-04 15:20:20', '', 0, 'http://localhost/projetos/wepayout_site/?post_type=problema&#038;p=49', 0, 'problema', '', 0),
(50, 1, '2019-09-04 12:20:30', '2019-09-04 15:20:30', '', 'Resolva', '', 'publish', 'closed', 'closed', '', 'resolva', '', '', '2019-09-04 12:20:30', '2019-09-04 15:20:30', '', 0, 'http://localhost/projetos/wepayout_site/?post_type=problema&#038;p=50', 0, 'problema', '', 0),
(51, 1, '2019-09-04 12:20:42', '2019-09-04 15:20:42', '', 'Elimine', '', 'publish', 'closed', 'closed', '', 'elimine', '', '', '2019-09-04 12:20:42', '2019-09-04 15:20:42', '', 0, 'http://localhost/projetos/wepayout_site/?post_type=problema&#038;p=51', 0, 'problema', '', 0),
(52, 1, '2019-09-04 12:20:56', '2019-09-04 15:20:56', '', 'Economize', '', 'publish', 'closed', 'closed', '', 'ecomomize', '', '', '2019-09-04 12:21:44', '2019-09-04 15:21:44', '', 0, 'http://localhost/projetos/wepayout_site/?post_type=problema&#038;p=52', 0, 'problema', '', 0),
(53, 1, '2019-09-04 12:21:50', '0000-00-00 00:00:00', '', 'Rascunho automático', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2019-09-04 12:21:50', '0000-00-00 00:00:00', '', 0, 'http://localhost/projetos/wepayout_site/?post_type=pagamento&p=53', 0, 'pagamento', '', 0),
(54, 1, '2019-09-04 12:22:59', '0000-00-00 00:00:00', '', 'Rascunho automático', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2019-09-04 12:22:59', '0000-00-00 00:00:00', '', 0, 'http://localhost/projetos/wepayout_site/?post_type=pagamento&p=54', 0, 'pagamento', '', 0),
(55, 1, '2019-09-04 12:25:11', '0000-00-00 00:00:00', '', 'Rascunho automático', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2019-09-04 12:25:11', '0000-00-00 00:00:00', '', 0, 'http://localhost/projetos/wepayout_site/?post_type=pagamento&p=55', 0, 'pagamento', '', 0),
(56, 1, '2019-09-04 12:26:29', '2019-09-04 15:26:29', '', 'Integre com nosso  sistema', '', 'publish', 'closed', 'closed', '', 'integre-com-nosso-sistema', '', '', '2019-09-04 12:26:29', '2019-09-04 15:26:29', '', 0, 'http://localhost/projetos/wepayout_site/?post_type=pagamento&#038;p=56', 0, 'pagamento', '', 0),
(57, 1, '2019-09-04 12:26:26', '2019-09-04 15:26:26', '', 's1', '', 'inherit', 'open', 'closed', '', 's1-2', '', '', '2019-09-04 12:26:26', '2019-09-04 15:26:26', '', 56, 'http://localhost/projetos/wepayout_site/wp-content/uploads/2019/09/s1-1.png', 0, 'attachment', 'image/png', 0),
(58, 1, '2019-09-04 12:26:51', '2019-09-04 15:26:51', '', 'Deposite os fundos', '', 'publish', 'closed', 'closed', '', 'deposite-os-fundos', '', '', '2019-09-04 12:26:51', '2019-09-04 15:26:51', '', 0, 'http://localhost/projetos/wepayout_site/?post_type=pagamento&#038;p=58', 0, 'pagamento', '', 0),
(59, 1, '2019-09-04 12:26:48', '2019-09-04 15:26:48', '', 's2', '', 'inherit', 'open', 'closed', '', 's2', '', '', '2019-09-04 12:26:48', '2019-09-04 15:26:48', '', 58, 'http://localhost/projetos/wepayout_site/wp-content/uploads/2019/09/s2.png', 0, 'attachment', 'image/png', 0),
(60, 1, '2019-09-04 12:27:18', '2019-09-04 15:27:18', '', 'Envie as instruções de pagamentos', '', 'publish', 'closed', 'closed', '', 'envie-as-instrucoes-de-pagamentos', '', '', '2019-09-04 12:51:27', '2019-09-04 15:51:27', '', 0, 'http://localhost/projetos/wepayout_site/?post_type=pagamento&#038;p=60', 0, 'pagamento', '', 0),
(61, 1, '2019-09-04 12:27:10', '2019-09-04 15:27:10', '', 'Component 5', '', 'inherit', 'open', 'closed', '', 'component-5-2', '', '', '2019-09-04 12:27:10', '2019-09-04 15:27:10', '', 60, 'http://localhost/projetos/wepayout_site/wp-content/uploads/2019/09/Component-5-1.png', 0, 'attachment', 'image/png', 0),
(62, 1, '2019-09-04 12:27:41', '2019-09-04 15:27:41', '', 'Processamos os pagamentos', '', 'publish', 'closed', 'closed', '', 'processamos-os-pagamentos', '', '', '2019-09-04 12:27:41', '2019-09-04 15:27:41', '', 0, 'http://localhost/projetos/wepayout_site/?post_type=pagamento&#038;p=62', 0, 'pagamento', '', 0),
(63, 1, '2019-09-04 12:27:38', '2019-09-04 15:27:38', '', 'e4', '', 'inherit', 'open', 'closed', '', 'e4-2', '', '', '2019-09-04 12:27:38', '2019-09-04 15:27:38', '', 62, 'http://localhost/projetos/wepayout_site/wp-content/uploads/2019/09/e4-1.png', 0, 'attachment', 'image/png', 0),
(64, 1, '2019-09-04 12:28:04', '2019-09-04 15:28:04', '', 'Pagamento creditado', '', 'publish', 'closed', 'closed', '', 'pagamento-creditado', '', '', '2019-09-04 12:28:04', '2019-09-04 15:28:04', '', 0, 'http://localhost/projetos/wepayout_site/?post_type=pagamento&#038;p=64', 0, 'pagamento', '', 0),
(65, 1, '2019-09-04 12:28:00', '2019-09-04 15:28:00', '', 's5', '', 'inherit', 'open', 'closed', '', 's5', '', '', '2019-09-04 12:28:00', '2019-09-04 15:28:00', '', 64, 'http://localhost/projetos/wepayout_site/wp-content/uploads/2019/09/s5.png', 0, 'attachment', 'image/png', 0),
(66, 1, '2019-09-04 12:51:25', '2019-09-04 15:51:25', '', 's3', '', 'inherit', 'open', 'closed', '', 's3', '', '', '2019-09-04 12:51:25', '2019-09-04 15:51:25', '', 60, 'http://localhost/projetos/wepayout_site/wp-content/uploads/2019/09/s3.png', 0, 'attachment', 'image/png', 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `wp_termmeta`
--

DROP TABLE IF EXISTS `wp_termmeta`;
CREATE TABLE IF NOT EXISTS `wp_termmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`meta_id`),
  KEY `term_id` (`term_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `wp_terms`
--

DROP TABLE IF EXISTS `wp_terms`;
CREATE TABLE IF NOT EXISTS `wp_terms` (
  `term_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`term_id`),
  KEY `slug` (`slug`(191)),
  KEY `name` (`name`(191))
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `wp_terms`
--

INSERT INTO `wp_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
(1, 'Sem categoria', 'sem-categoria', 0),
(3, 'Menu 1', 'menu-1', 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `wp_term_relationships`
--

DROP TABLE IF EXISTS `wp_term_relationships`;
CREATE TABLE IF NOT EXISTS `wp_term_relationships` (
  `object_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  KEY `term_taxonomy_id` (`term_taxonomy_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `wp_term_relationships`
--

INSERT INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
(1, 1, 0),
(14, 3, 0),
(15, 3, 0),
(16, 3, 0),
(17, 3, 0),
(18, 3, 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `wp_term_taxonomy`
--

DROP TABLE IF EXISTS `wp_term_taxonomy`;
CREATE TABLE IF NOT EXISTS `wp_term_taxonomy` (
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`term_taxonomy_id`),
  UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  KEY `taxonomy` (`taxonomy`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `wp_term_taxonomy`
--

INSERT INTO `wp_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 1),
(3, 3, 'nav_menu', '', 0, 5);

-- --------------------------------------------------------

--
-- Estrutura da tabela `wp_usermeta`
--

DROP TABLE IF EXISTS `wp_usermeta`;
CREATE TABLE IF NOT EXISTS `wp_usermeta` (
  `umeta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`umeta_id`),
  KEY `user_id` (`user_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=MyISAM AUTO_INCREMENT=28 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `wp_usermeta`
--

INSERT INTO `wp_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'nickname', 'wepayout'),
(2, 1, 'first_name', ''),
(3, 1, 'last_name', ''),
(4, 1, 'description', ''),
(5, 1, 'rich_editing', 'true'),
(6, 1, 'syntax_highlighting', 'true'),
(7, 1, 'comment_shortcuts', 'false'),
(8, 1, 'admin_color', 'fresh'),
(9, 1, 'use_ssl', '0'),
(10, 1, 'show_admin_bar_front', 'true'),
(11, 1, 'locale', ''),
(12, 1, 'wp_capabilities', 'a:1:{s:13:\"administrator\";b:1;}'),
(13, 1, 'wp_user_level', '10'),
(14, 1, 'dismissed_wp_pointers', ''),
(15, 1, 'show_welcome_panel', '1'),
(16, 1, 'session_tokens', 'a:4:{s:64:\"2e5ef24adf4b1b521cfb605b2c39276597d102cecd08a12089579f2810040d79\";a:4:{s:10:\"expiration\";i:1567708230;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:115:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36\";s:5:\"login\";i:1567535430;}s:64:\"b0d6c0a3e379caacdf461ad3739a572d03e12842fa25ce7415c54d5ef29e0f50\";a:4:{s:10:\"expiration\";i:1567711387;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:115:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36\";s:5:\"login\";i:1567538587;}s:64:\"64adb9e84b0e2c0eea40a8bd053408980783b19be18ebf5b4494c8801f7cf903\";a:4:{s:10:\"expiration\";i:1567712337;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:115:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36\";s:5:\"login\";i:1567539537;}s:64:\"4bad51f2efd13ef10815bf1f326341fcb3cd1e1e7770287867fe2af0370e3012\";a:4:{s:10:\"expiration\";i:1567788567;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:115:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36\";s:5:\"login\";i:1567615767;}}'),
(17, 1, 'wp_dashboard_quick_press_last_post_id', '4'),
(18, 1, 'wp_r_tru_u_x', 'a:2:{s:2:\"id\";i:0;s:7:\"expires\";i:1567624767;}'),
(19, 1, 'wp_r_tru_u_x', 'a:2:{s:2:\"id\";i:0;s:7:\"expires\";i:1567624767;}'),
(20, 1, 'last_login_time', '2019-09-04 13:49:27'),
(21, 1, 'managenav-menuscolumnshidden', 'a:4:{i:0;s:11:\"link-target\";i:1;s:15:\"title-attribute\";i:2;s:3:\"xfn\";i:3;s:11:\"description\";}'),
(22, 1, 'metaboxhidden_nav-menus', 'a:2:{i:0;s:22:\"add-post-type-destaque\";i:1;s:12:\"add-post_tag\";}'),
(23, 1, 'wp_user-settings', 'libraryContent=browse&editor=html'),
(24, 1, 'wp_user-settings-time', '1567604870'),
(25, 1, 'nav_menu_recently_edited', '3'),
(26, 1, 'closedpostboxes_servico', 'a:0:{}'),
(27, 1, 'metaboxhidden_servico', 'a:1:{i:0;s:7:\"slugdiv\";}');

-- --------------------------------------------------------

--
-- Estrutura da tabela `wp_users`
--

DROP TABLE IF EXISTS `wp_users`;
CREATE TABLE IF NOT EXISTS `wp_users` (
  `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_login` varchar(60) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`ID`),
  KEY `user_login_key` (`user_login`),
  KEY `user_nicename` (`user_nicename`),
  KEY `user_email` (`user_email`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `wp_users`
--

INSERT INTO `wp_users` (`ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
(1, 'wepayout', '$P$BXb/Gib2xS2cKbz4/fSPeT8.D6Eonb/', 'wepayout', 'carolinoamandacs@gmail.com', '', '2019-09-03 18:30:16', '', 0, 'wepayout');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
